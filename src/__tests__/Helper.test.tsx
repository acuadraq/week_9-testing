import filterId from '../helpers/filterId';

test('Testing filter Id', () => {
  const bookmark = [
    {
      bookmark: {
        id: 2,
        name: 'Spiderman',
        thumbnail: 'placeholder.png',
        type: 'comics',
      },
    },
  ];

  expect(filterId(bookmark, 2)).toEqual(0);
  expect(filterId(bookmark, 10)).toEqual(-1);
});
