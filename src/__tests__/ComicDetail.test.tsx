/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, waitFor, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { rest } from 'msw';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import server from '../mocks/server';
import ComicDetail from '../components/ComicDetail/ComicDetail';
import { store } from '../redux/store';
import Home from '../components/Home/Home';

describe('Comic Detail page test', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });

  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/comics/93571']}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="comics/:id" element={<ComicDetail />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );
  });

  it('Testing if the comic detail renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Venom and Carnage');
    component.getByText('Eddie Brock');
    expect(component.queryByLabelText('cover from Venom/Carnage')).toBeTruthy();
  });

  it('Testing if saving comic to bookmark', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByLabelText('save-item');
    userEvent.click(button);
    expect(component.getByLabelText('item-saved')).toBeTruthy();
  });

  it('Testing if hiding function works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByLabelText('hide-item');
    userEvent.click(button);
    await waitFor(() => {
      component.getByText('Page Not Found');
    });
  });

  it('Handling comic not found', async () => {
    server.use(
      rest.get('https://gateway.marvel.com/v1/public/comics/93571', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json({ code: 404, status: 'We could not find the comic' }));
      }),
    );

    await waitFor(() => {
      component.getByText('Page Not Found');
    });
  });
});
