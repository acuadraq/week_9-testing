/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import server from '../mocks/server';
import renderWithRouter from '../utils/Wrapper';
import RoutesNames from '../customroutes/routesNames';
import Comics from '../components/Comics/Comics';

describe('Comics Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    server.resetHandlers();
    cleanup();
  });

  beforeEach(() => {
    component = renderWithRouter(<Comics />, RoutesNames.comics);
  });

  it('Testing if the list of comics render', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByLabelText('Spiderman Comic');
  });

  it('Testing search feature', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.type(screen.getByRole('textbox'), 'loki');
    expect(screen.getByRole('textbox')).toHaveValue('loki');
    await waitFor(() => {
      expect(screen.queryByLabelText('Loki First Comic')).toBeTruthy();
    });
  });

  it('Testing format select', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.selectOptions(screen.getByLabelText('format-select'), 'Graphic Novel');
    await waitFor(() => {
      expect(screen.queryByLabelText('Black Panther')).toBeTruthy();
    });
  });

  it('Testing the useEffect for paginate', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const page = screen.queryByLabelText('Page 2');
    if (page) userEvent.click(page);
    expect(page).toHaveClass('currentPage');
    await waitFor(() => {
      expect(screen.queryByLabelText('Ant-Man')).toBeTruthy();
    });
    userEvent.selectOptions(screen.getByLabelText('format-select'), 'Graphic Novel');
    await waitFor(() => {
      expect(screen.queryByLabelText('Black Panther')).toBeTruthy();
    });
    expect(screen.queryByLabelText('Page 1')).toHaveClass('currentPage');
  });
});
