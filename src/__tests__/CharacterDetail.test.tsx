/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, waitFor, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { rest } from 'msw';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import server from '../mocks/server';
import CharacterDetail from '../components/CharacterDetail/CharacterDetail';
import { store } from '../redux/store';
import Home from '../components/Home/Home';

describe('Character Detail Page tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });

  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/characters/1009368']}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="characters/:id" element={<CharacterDetail />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );
  });

  it('Testing if the character detail renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Iron Man');
    expect(component.queryByLabelText('Shang-Chi (2021) #5')).toBeTruthy();
    expect(component.queryByLabelText('X-MEN (2004) #186')).toBeTruthy();
  });

  it('Testing if redux works by saving it to bookmark', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByLabelText('save-item');
    userEvent.click(button);
    expect(component.getByLabelText('item-saved')).toBeTruthy();
  });

  it('Testing if hiding function works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByLabelText('hide-item');
    userEvent.click(button);
    await waitFor(() => {
      component.getByText('Page Not Found');
    });
  });

  it('Handling character not found', async () => {
    server.use(
      rest.get('https://gateway.marvel.com/v1/public/characters/1009368', (req, res, ctx) => {
        return res(
          ctx.status(200),
          ctx.json({ code: 404, status: 'We could not find the caracter' }),
        );
      }),
    );

    await waitFor(() => {
      component.getByText('Page Not Found');
    });
  });
});
