import { cleanup } from '@testing-library/react';
import { Bookmark } from '../redux/actions/actions';
import ActionTypes from '../redux/constants/action-types';
import { bookmarkReducer, hideReducer } from '../redux/reducers/dataReducer';

describe('Testing all the reducers', () => {
  afterEach(() => cleanup());

  it('Should return the initial state', () => {
    expect(bookmarkReducer(undefined, undefined)).toEqual([]);
  });

  const bookmark: Bookmark = {
    bookmark: {
      id: 1,
      name: 'Ironman',
      thumbnail: 'placeholder.png',
      type: 'characters',
    },
  };

  it('Should add a bookmark', () => {
    expect(
      bookmarkReducer(undefined, { type: ActionTypes.SET_BOOKMARK, payload: { bookmark } }),
    ).toEqual([
      {
        bookmark: {
          id: 1,
          name: 'Ironman',
          thumbnail: 'placeholder.png',
          type: 'characters',
        },
        ...[],
      },
    ]);
  });

  it('Should not add duplicates of an item', () => {
    expect(
      bookmarkReducer([bookmark], { type: ActionTypes.SET_BOOKMARK, payload: { bookmark } }),
    ).toEqual([bookmark]);
  });

  const initialState: Bookmark[] = [
    {
      bookmark: {
        id: 1,
        name: 'Ironman',
        thumbnail: 'placeholder.png',
        type: 'characters',
      },
    },
    {
      bookmark: {
        id: 2,
        name: 'Spiderman',
        thumbnail: 'placeholder.png',
        type: 'comic',
      },
    },
  ];
  it('Should delete one bookmark', () => {
    expect(
      bookmarkReducer(initialState, { type: ActionTypes.REMOVE_BOOKMARK, payload: { id: 0 } }),
    ).toEqual([
      {
        bookmark: {
          id: 2,
          name: 'Spiderman',
          thumbnail: 'placeholder.png',
          type: 'comic',
        },
        ...[],
      },
    ]);
  });

  it('Should delete all bookmarks', () => {
    expect(bookmarkReducer(initialState, { type: ActionTypes.REMOVE_ALL_BOOKMARKS })).toEqual([]);
  });

  it('Should return the initial state from hideReducer', () => {
    expect(hideReducer(undefined, undefined)).toEqual([]);
  });

  it('Should add a number to the state', () => {
    expect(hideReducer(undefined, { type: ActionTypes.SET_HIDE, payload: { id: 1002 } })).toEqual([
      1002,
    ]);
  });
});
