/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import server from '../mocks/server';
import Bookmarks from '../components/Bookmark/Bookmarks';
import RoutesNames from '../customroutes/routesNames';
import persistedReducer from '../redux/reducers/index';

describe('Bookmark page tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });

  const initialState = [
    {
      bookmark: {
        id: 1010699,
        name: 'Aaron Stack',
        thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg',
        type: 'characters',
      },
    },
    {
      bookmark: {
        id: 1009368,
        name: 'Iron Man',
        thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55.jpg',
        type: 'characters',
      },
    },
    {
      bookmark: {
        id: 47774,
        name: 'Galactus the Devourer',
        thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/53974b25a91d0.jpg',
        type: 'comics',
      },
    },
  ];

  const store = createStore(persistedReducer, { bookmarks: initialState });

  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/bookmarks/']}>
          <Routes>
            <Route path={RoutesNames.bookmarks} element={<Bookmarks />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );
  });

  it('Testing if bookmarks render correctly with the initial state', () => {
    component.getByText('Aaron Stack');
    component.getByText('Galactus the Devourer');
    component.getByText('Iron Man');
  });

  it('Testing the delete button with item #2', () => {
    const button = component.queryByLabelText('delete-1009368');
    if (button) userEvent.click(button);
    expect(component.queryByText('Iron Man')).toBeFalsy();
  });

  it('Testing the remove all button', () => {
    const button = component.getByText('Remove All');
    userEvent.click(button);
    component.getByText('There is no bookmarks yet');
  });
});
