import React from 'react';
import Home from '../components/Home/Home';
import renderWithRouter from '../utils/Wrapper';

test('Check if Home renders', () => {
  const component = renderWithRouter(<Home />);
  component.getByText('View All');
});
