import React from 'react';
import { screen, fireEvent } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import Navbar from '../components/Navbar';
import renderWithRouter from '../utils/Wrapper';

describe('Navbar Tests', () => {
  let component: RenderResult;

  beforeEach(() => {
    component = renderWithRouter(<Navbar />);
  });

  test('Check if link redirect you to Characters', () => {
    const anchor = component.getByText('Characters');
    expect(anchor).toHaveAttribute('href', '/characters');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/characters');
  });

  test('Check if link redirect you to Comics', () => {
    const anchor = component.getByText('Comics');
    expect(anchor).toHaveAttribute('href', '/comics');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/comics');
  });

  test('Check if link redirect you to Stories', () => {
    const anchor = component.getByText('Stories');
    expect(anchor).toHaveAttribute('href', '/stories');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/stories');
  });

  test('Check if link redirect you to Bookmarks', () => {
    const anchor = component.getByText('Bookmarks');
    expect(anchor).toHaveAttribute('href', '/bookmarks');
    fireEvent.click(anchor, { click: 0 });
    expect(screen.getByTestId('location-display')).toHaveTextContent('/bookmarks');
  });
});
