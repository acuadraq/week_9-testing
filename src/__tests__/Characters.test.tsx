/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import server from '../mocks/server';
import Characters from '../components/Characters/Characters';
import renderWithRouter from '../utils/Wrapper';
import RoutesNames from '../customroutes/routesNames';

describe('Characters Component tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    server.resetHandlers();
    cleanup();
  });

  beforeEach(() => {
    component = renderWithRouter(<Characters />, RoutesNames.characters);
  });

  it('Testing if the list of characters render', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('A-Bomb (HAS)');
  });

  it('Testing search feature', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.type(screen.getByRole('textbox'), 'wolverine');
    expect(screen.getByRole('textbox')).toHaveValue('wolverine');
    await waitFor(() => {
      expect(screen.queryByText('Wolverine')).toBeTruthy();
    });
  });

  it('Testing comics select', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.selectOptions(screen.getByLabelText('comic-select'), 'Marvel Previews (2017)');
    await waitFor(() => {
      expect(screen.queryByText('Spider-Man (Ultimate)')).toBeTruthy();
    });
  });

  it('Testing stories select', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.selectOptions(screen.getByLabelText('stories-select'), 'Investigating murder');
    await waitFor(() => {
      expect(screen.queryByText('Alpha Flight')).toBeTruthy();
    });
  });

  it('Testing the pagination', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const page = screen.queryByLabelText('Page 2');
    if (page) userEvent.click(page);
    expect(page).toHaveClass('currentPage');
    await waitFor(() => {
      expect(screen.queryByText('Adam Destine')).toBeTruthy();
    });
  });

  it('Testing the useEffect for paginate', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const page = screen.queryByLabelText('Page 2');
    if (page) userEvent.click(page);
    expect(page).toHaveClass('currentPage');
    await waitFor(() => {
      expect(screen.queryByText('Adam Destine')).toBeTruthy();
    });
    userEvent.selectOptions(screen.getByLabelText('stories-select'), 'Investigating murder');
    await waitFor(() => {
      expect(screen.queryByText('Alpha Flight')).toBeTruthy();
    });
    expect(screen.queryByLabelText('Page 1')).toHaveClass('currentPage');
  });
});
