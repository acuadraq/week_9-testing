/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, waitFor, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { rest } from 'msw';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import server from '../mocks/server';
import StoryDetail from '../components/StoryDetail/StoryDetail';
import { store } from '../redux/store';
import Home from '../components/Home/Home';

describe('Story deatil page tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    cleanup();
    server.resetHandlers();
  });

  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/stories/25637']}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="stories/:id" element={<StoryDetail />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );
  });

  it('Testing if the story detail renders', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByText('Universe X (2000) #12');
    component.getByText('Adam Warlock');
    expect(component.queryByLabelText('Universe X (2000) #12')).toBeTruthy();
  });

  it('Testing if saving story to bookmark', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByLabelText('save-item');
    userEvent.click(button);
    expect(component.getByLabelText('item-saved')).toBeTruthy();
  });

  it('Testing if hiding function works', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const button = component.getByLabelText('hide-item');
    userEvent.click(button);
    await waitFor(() => {
      component.getByText('Page Not Found');
    });
  });

  it('Handling story not found', async () => {
    server.use(
      rest.get('https://gateway.marvel.com/v1/public/stories/25637', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json({ code: 404, status: 'We could not find the story' }));
      }),
    );

    await waitFor(() => {
      component.getByText('Page Not Found');
    });
  });
});
