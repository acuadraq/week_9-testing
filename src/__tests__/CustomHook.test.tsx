import { renderHook, act } from '@testing-library/react-hooks';
import useRefresh from '../customhooks/useRefresh';
import RoutesNames from '../customroutes/routesNames';

const mockNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => mockNavigate,
}));

describe('Testing custom hook useRefresh', () => {
  const subject = () => renderHook(() => useRefresh(mockNavigate, RoutesNames.bookmarks));

  it('returns a function', () => {
    const { result } = subject();

    expect(typeof result.current).toBe('function');
  });

  it('clears the timeout', () => {
    jest.useFakeTimers();

    const { result, unmount } = subject();

    result.current();

    act(() => {
      jest.runAllTimers();
    });

    unmount();
    expect(clearTimeout).toHaveBeenCalledTimes(1);
  });
});
