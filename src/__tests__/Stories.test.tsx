/* eslint-disable react/react-in-jsx-scope */
import { cleanup, RenderResult, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import server from '../mocks/server';
import renderWithRouter from '../utils/Wrapper';
import RoutesNames from '../customroutes/routesNames';
import Stories from '../components/Storie/Stories';

describe('Stories Component Tests', () => {
  let component: RenderResult;

  beforeAll(() => server.listen());
  afterAll(() => server.close());

  afterEach(() => {
    server.resetHandlers();
    cleanup();
  });

  beforeEach(() => {
    component = renderWithRouter(<Stories />, RoutesNames.stories);
  });

  it('Testing if the list of stories render', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    component.getByLabelText('Investigating m');
  });

  it('Testing the search feature', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    userEvent.type(screen.getByRole('textbox'), 'investigating');
    expect(screen.getByRole('textbox')).toHaveValue('investigating');

    expect(screen.queryByLabelText('Investigating m')).toBeTruthy();
  });

  it('Testing the pagination', async () => {
    await waitFor(() => {
      expect(component.queryByLabelText(/Loading/)).toEqual(null);
    });
    const page = screen.queryByLabelText('Page 2');
    if (page) userEvent.click(page);
    expect(page).toHaveClass('currentPage');
    await waitFor(() => {
      expect(screen.queryByLabelText('Maya Lopez')).toBeTruthy();
    });
  });
});
