import React from 'react';
import { Outlet } from 'react-router-dom';
import Footer from '../components/Footer';

const SecondRoute = () => {
  return (
    <>
      <Outlet />
      <Footer />
    </>
  );
};
export default SecondRoute;
