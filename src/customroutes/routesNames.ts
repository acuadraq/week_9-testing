const RoutesNames = {
  home: '/',
  characters: 'characters',
  charactesDetail: 'characters/:id',
  comics: 'comics',
  comicsDetail: 'comics/:id',
  stories: 'stories',
  storiesDetail: 'stories/:id',
  bookmarks: 'bookmarks',
};

export default RoutesNames;
