/* eslint-disable quote-props */
const comicFormatFilter = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '267c367ac727f39fa14d77f2ef8fe884ea4a4281',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 59,
    'count': 5,
    'results': [
      {
        'id': 66225,
        'digitalId': 0,
        'title': 'Black Panther',
        'issueNumber': 0,
        'variantDescription': '',
        'description':
          'When the cybernetic artificial intelligence, Machinesmith, infiltrates all of Wakanda’s systems, the very existence of human civilization is threatened. It is up to six Wakandan science apprentices and six Takumi Masters, working with Black Panther and his sister, Shuri, to create the ultimate machine to stop Machinesmith’s flawed, but deeply felt, vision for integrating man and technology into harmonious perfection. Collecting Black Panther: Soul of a Machine Chapters #1-8.',
        'modified': '2018-01-09T15:44:33-0500',
        'isbn': '',
        'upc': '75960605301808855',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Graphic Novel',
        'pageCount': 88,
        'textObjects': [
          {
            'type': 'issue_solicit_text',
            'language': 'en-us',
            'text':
              'When the cybernetic artificial intelligence, Machinesmith, infiltrates all of Wakanda’s systems, the very existence of human civilization is threatened. It is up to six Wakandan science apprentices and six Takumi Masters, working with Black Panther and his sister, Shuri, to create the ultimate machine to stop Machinesmith’s flawed, but deeply felt, vision for integrating man and technology into harmonious perfection. Collecting Black Panther: Soul of a Machine Chapters #1-8.',
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66225',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/66225/black_panther_soul_of_a_machine_graphic_novel?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/24010',
          'name': 'Black Panther: Soul of a Machine (2018)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2018-01-12T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2017-12-16T00:00:00-0500',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 0,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/60/5a5527eee81ca',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/60/5a5527eee81ca',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 16,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/66225/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12401',
              'name': 'Sotocolor',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13155',
              'name': 'Mike Bowden',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12502',
              'name': 'Andrea Divito',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12702',
              'name': 'J.L. Giles',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/745',
              'name': 'Yvel Guichet',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13245',
              'name': 'Jose Luis',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12847',
              'name': 'Chuck Brown',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/6001',
              'name': 'Fabian Nicieza',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12886',
              'name': 'Geoffrey Thorne',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12919',
              'name': 'Katherine Brown',
              'role': 'project manager',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/3675',
              'name': 'Keith Champagne',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13289',
              'name': 'Adriana Di Benedetto',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12978',
              'name': 'Roberto Poggi',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/1082',
              'name': 'Ariel Olivetti',
              'role': 'penciler (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/8775',
              'name': 'Joe Sabino',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12757',
              'name': 'Darren Sanchez',
              'role': 'editor',
            },
          ],
          'returned': 16,
        },
        'characters': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/66225/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009187',
              'name': 'Black Panther',
            },
          ],
          'returned': 1,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/66225/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/143593',
              'name': 'cover from CUSTOM PROMO LEXUS 2017-2018 BLACK PANTHER GN (2018) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/143594',
              'name': 'story from CUSTOM PROMO LEXUS 2017-2018 BLACK PANTHER GN (2018) #1',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/66225/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 40921,
        'digitalId': 0,
        'title': 'Ghost Rider: Official Index to the Marvel Universe GN-TPB (Graphic Novel)',
        'issueNumber': 1,
        'variantDescription': '',
        'description':
          'The complete history of the Spirit of Vengeance from\r\nhis earliest appearances to the present day! This\r\nfact-packed volume chronicles every character, team,\r\nplace and piece of equipment &mdash; and provides vital\r\ninformation about all things Ghost Rider! Collecting\r\nthe Ghost Rider material from WOLVERINE, PUNISHER\r\n& GHOST RIDER: OFFICIAL INDEX TO THE MARVEL\r\nUNIVERSE #1-6.\r\n152 PGS./Rated T+ &#133;$19.99',
        'modified': '2012-01-30T01:31:01-0500',
        'isbn': '978-0-7851-6200-1',
        'upc': '5960616200-00111',
        'diamondCode': '',
        'ean': '9780785 162001 51999',
        'issn': '',
        'format': 'Graphic Novel',
        'pageCount': 152,
        'textObjects': [
          {
            'type': 'issue_solicit_text',
            'language': 'en-us',
            'text':
              'The complete history of the Spirit of Vengeance from\r\nhis earliest appearances to the present day! This\r\nfact-packed volume chronicles every character, team,\r\nplace and piece of equipment &mdash; and provides vital\r\ninformation about all things Ghost Rider! Collecting\r\nthe Ghost Rider material from WOLVERINE, PUNISHER\r\n& GHOST RIDER: OFFICIAL INDEX TO THE MARVEL\r\nUNIVERSE #1-6.\r\n152 PGS./Rated T+ &#133;$19.99',
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40921',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/40921/ghost_rider_official_index_to_the_marvel_universe_gn-tpb_graphic_novel?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/15136',
          'name': 'Ghost Rider: Official Index to the Marvel Universe GN-TPB (2011 - Present)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2012-02-01T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2012-01-17T00:00:00-0500',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 19.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/a0/4ec2b1272cee0',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/a0/4ec2b1272cee0',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/40921/creators',
          'items': [],
          'returned': 0,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/40921/characters',
          'items': [],
          'returned': 0,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/40921/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92664',
              'name': 'Cover #92664',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92665',
              'name': 'Interior #92665',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/40921/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 38292,
        'digitalId': 0,
        'title': 'New X-Men Vol. 8: Here Comes Tomorrow GN-TPB (Graphic Novel)',
        'issueNumber': 1,
        'variantDescription': '',
        'description':
          'Legendary artist Marc Silvestri returns to the X-Men as Grant Morrison brings his mind-bending run to a close! In a tale inspired by the classic "Days of Future Past" storyline, meet the X-Men from 150 years in our future. While some faces may seem familiar, readers will discover all-new heroes and villains for the first time. What incredible force threatens to destroy the mutants of the future, and how will it affect the X-Men of today? Collecting NEW X-MEN (2001) #151-154.',
        'modified': '2011-12-13T01:33:22-0500',
        'isbn': '978-0-7851-5539-3',
        'upc': '5960615539-00111',
        'diamondCode': '',
        'ean': '9780785 155393 51499',
        'issn': '',
        'format': 'Graphic Novel',
        'pageCount': 112,
        'textObjects': [
          {
            'type': 'issue_solicit_text',
            'language': 'en-us',
            'text':
              'Legendary artist Marc Silvestri returns to the X-Men as Grant Morrison brings his mind-bending run to a close! In a tale inspired by the classic "Days of Future Past" storyline, meet the X-Men from 150 years in our future. While some faces may seem familiar, readers will discover all-new heroes and villains for the first time. What incredible force threatens to destroy the mutants of the future, and how will it affect the X-Men of today? Collecting NEW X-MEN (2001) #151-154.',
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38292',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/38292/new_x-men_vol_8_here_comes_tomorrow_gn-tpb_graphic_novel?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/13793',
          'name': 'New X-Men Vol. 8: Here Comes Tomorrow GN-TPB (2011 - Present)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2011-12-14T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2011-11-29T00:00:00-0500',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 14.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/e0/4e7ba2a15bf18',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/e0/4e7ba2a15bf18',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/38292/creators',
          'items': [],
          'returned': 0,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/38292/characters',
          'items': [],
          'returned': 0,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/38292/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/94862',
              'name': 'NEW X-MEN',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/94863',
              'name': 'NEW X-MEN vol 8',
              'type': 'cover',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/38292/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 1288,
        'digitalId': 0,
        'title': 'New X-Men Vol. 7: Planet X GN-TPB (Graphic Novel)',
        'issueNumber': 0,
        'variantDescription': '',
        'description':
          "The X-Men have long been at odds with one man who has caused them so much torment throughout the years: the genetic terrorist known as Magneto, Master of Magnetism. They have battled him on countless occasions as he fought against Charles Xavier's dream of peaceful coexistence between mutants and humans, preferring instead that mutants inherit the Earth. At last, the X-Men's greatest adversary was thought dead, finally ridding the world of his mutant menace. But instead, he was simply hiding, waiting in secret to make a move against the team that has thwarted his plans again and again. But where he was hiding will shatter the lives of everyone at Xavier's Mansion. If they even survive his latest attack, can the X-Men prevent his planned genocide of the entire human race? Collecting NEW X-MEN (2001) #146-150. ",
        'modified': '2020-02-04T11:07:40-0500',
        'isbn': '',
        'upc': '',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Graphic Novel',
        'pageCount': 136,
        'textObjects': [
          {
            'type': 'issue_solicit_text',
            'language': 'en-us',
            'text':
              "The X-Men have long been at odds with one man who has caused them so much torment throughout the years: the genetic terrorist known as Magneto, Master of Magnetism. They have battled him on countless occasions as he fought against Charles Xavier's dream of peaceful coexistence between mutants and humans, preferring instead that mutants inherit the Earth. At last, the X-Men's greatest adversary was thought dead, finally ridding the world of his mutant menace. But instead, he was simply hiding, waiting in secret to make a move against the team that has thwarted his plans again and again. But where he was hiding will shatter the lives of everyone at Xavier's Mansion. If they even survive his latest attack, can the X-Men prevent his planned genocide of the entire human race? Collecting NEW X-MEN (2001) #146-150. ",
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/1288',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/1288/new_x-men_vol_7_planet_x_gn-tpb_graphic_novel?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/New-X-Men-Vol-7-Here-Comes-Tomorrow-0/digital-comic/30663?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/339',
          'name': 'New X-Men Vol. 7: Planet X GN-TPB (2011)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/420',
            'name': 'New X-Men (2001) #154',
          },
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/120',
            'name': 'New X-Men (2001) #153',
          },
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/121',
            'name': 'New X-Men (2001) #152',
          },
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/122',
            'name': 'New X-Men (2001) #151',
          },
        ],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2011-11-04T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '1961-01-01T00:00:00-0500',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 14.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/4/70/5e39980885cb5',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/40/5e56a3c63c3b6',
            'extension': 'jpg',
          },
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/5/e0/5e56a3c25330d',
            'extension': 'jpg',
          },
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5e56a3be4278f',
            'extension': 'jpg',
          },
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/7/50/5e56a3b9b9759',
            'extension': 'jpg',
          },
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/4/70/5e39980885cb5',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/1288/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/64',
              'name': 'Grant Morrison',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/5044',
              'name': 'Marc Silvestri',
              'role': 'penciller (cover)',
            },
          ],
          'returned': 2,
        },
        'characters': {
          'available': 6,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/1288/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009175',
              'name': 'Beast',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009478',
              'name': 'Cassandra Nova',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009327',
              'name': 'Jean Grey',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1011271',
              'name': 'New X-Men',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009718',
              'name': 'Wolverine',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009726',
              'name': 'X-Men',
            },
          ],
          'returned': 6,
        },
        'stories': {
          'available': 10,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/1288/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/459',
              'name':
                'Featuring the return of legendary X-Men artist Marc Silvestri. In a tale inspired by the classic',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/460',
              'name':
                'Featuring the return of legendary X-Men artist Marc Silvestri. In a tale inspired by the classic',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/608',
              'name': 'Cover #608',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/609',
              'name': 'Interior #609',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/900',
              'name': 'Cover #900',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/901',
              'name': 'Interior #901',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1046',
              'name': 'Cover #1046',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1047',
              'name': 'Interior #1047',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1490',
              'name': 'Cover #1490',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1491',
              'name': 'Interior #1491',
              'type': 'interiorStory',
            },
          ],
          'returned': 10,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/1288/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 37252,
        'digitalId': 0,
        'title': '15-Love GN-TPB (Graphic Novel)',
        'issueNumber': 1,
        'variantDescription': '',
        'description':
          "Teenage tennis action as only Mighty Marvel can deliver! Tennis is Mill Collins' whole life. But as the lowest-ranking student at the Wayde?Tennis Academy, she's about to lose her scholarship - and any chance at reaching her dream. Her coach has given up on her, her aunt thinks she doesn't try, and the only one who believes in her is a washed-up drunk. How wrong can things possibly go before Mill catches a break? Collecting 15-LOVE #1-3.",
        'modified': '2011-10-14T16:22:14-0400',
        'isbn': '978-0-7851-1334-8',
        'upc': '5960611334-00111',
        'diamondCode': '',
        'ean': '9780785 113348 51499',
        'issn': '',
        'format': 'Graphic Novel',
        'pageCount': 144,
        'textObjects': [
          {
            'type': 'issue_solicit_text',
            'language': 'en-us',
            'text':
              "Teenage tennis action as only Mighty Marvel can deliver! Tennis is Mill Collins' whole life. But as the lowest-ranking student at the Wayde?Tennis Academy, she's about to lose her scholarship - and any chance at reaching her dream. Her coach has given up on her, her aunt thinks she doesn't try, and the only one who believes in her is a washed-up drunk. How wrong can things possibly go before Mill catches a break? Collecting 15-LOVE #1-3.",
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/37252',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/37252/15-love_gn-tpb_graphic_novel?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/13380',
          'name': '15-Love GN-TPB (2013 - Present)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2011-10-19T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2011-10-04T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 14.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/50/4e272be1b96af',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/50/4e272be1b96af',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/37252/creators',
          'items': [],
          'returned': 0,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/37252/characters',
          'items': [],
          'returned': 0,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/37252/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/82482',
              'name': '15-Love GN-TPB (2013) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/82483',
              'name': 'Interior #82483',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/37252/events',
          'items': [],
          'returned': 0,
        },
      },
    ],
  },
};

export default comicFormatFilter;
