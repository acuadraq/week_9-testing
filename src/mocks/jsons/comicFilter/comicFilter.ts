/* eslint-disable quote-props */
const comicNameFilter = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': 'd7d0ae290396dab7eabfe305859f1c7d660c2640',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 52,
    'count': 5,
    'results': [
      {
        'id': 80467,
        'digitalId': 0,
        'title': 'Loki First Comic',
        'issueNumber': 0,
        'variantDescription': '',
        'description': null,
        'modified': '2020-01-03T16:49:15-0500',
        'isbn': '978-1-302-92031-9',
        'upc': '',
        'diamondCode': 'OCT191119',
        'ean': '9781302 920319 51799',
        'issn': '2374-6483',
        'format': 'Trade Paperback',
        'pageCount': 128,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/80467',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/80467/loki_the_god_who_fell_to_earth_trade_paperback?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/28358',
          'name': 'Loki: The God Who Fell To Earth (2020)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2020-01-08T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2019-11-18T00:00:00-0500',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 17.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/5/e0/5e0fa327a2652',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/5/e0/5e0fa327a2652',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 4,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80467/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12998',
              'name': 'Jan Bazaldua - Inactive',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13270',
              'name': 'Daniel Kibblesmith',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13585',
              'name': 'Ozgur Yildirim',
              'role': 'penciller (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4430',
              'name': 'Jeff Youngquist',
              'role': 'editor',
            },
          ],
          'returned': 4,
        },
        'characters': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80467/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009407',
              'name': 'Loki',
            },
          ],
          'returned': 1,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80467/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/178058',
              'name': 'cover from LOKI TPB (2020) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/178059',
              'name': 'story from LOKI TPB (2020) #1',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80467/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 80468,
        'digitalId': 0,
        'title': 'Loki: Agent Of Asgard - The Complete Collection (Trade Paperback)',
        'issueNumber': 0,
        'variantDescription': '',
        'description': null,
        'modified': '2019-12-13T16:17:11-0500',
        'isbn': '978-1-302-92073-9',
        'upc': '',
        'diamondCode': 'SEP191001',
        'ean': '9781302 920739 53999',
        'issn': '',
        'format': 'Trade Paperback',
        'pageCount': 504,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/80468',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/collection/80468/loki_agent_of_asgard_-_the_complete_collection_trade_paperback?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/28359',
          'name': 'Loki: Agent Of Asgard - The Complete Collection (2019)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2019-12-18T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2019-10-28T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 39.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/d0/5df3fc5f5e417',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/d0/5df3fc5f5e417',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80468/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/11401',
              'name': 'Various',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/648',
              'name': 'Simone Bianchi',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12823',
              'name': 'Jorge Coelho',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/11463',
              'name': 'Jason Aaron',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12174',
              'name': 'Al Ewing',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/8933',
              'name': 'Lee Garbett',
              'role': 'penciller (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4430',
              'name': 'Jeff Youngquist',
              'role': 'editor',
            },
          ],
          'returned': 7,
        },
        'characters': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80468/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009407',
              'name': 'Loki',
            },
          ],
          'returned': 1,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80468/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/178060',
              'name': 'cover from LOKI: AGENT OF ASGARD - THE COMPLETE COLLECTION TPB (2019) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/178061',
              'name': 'story from LOKI: AGENT OF ASGARD - THE COMPLETE COLLECTION TPB (2019) #1',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/80468/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 79421,
        'digitalId': 53004,
        'title': 'Loki (2019) #5',
        'issueNumber': 5,
        'variantDescription': '',
        'description': null,
        'modified': '2019-11-14T09:15:50-0500',
        'isbn': '',
        'upc': '75960609514800511',
        'diamondCode': 'SEP190903',
        'ean': '',
        'issn': '2374-6483',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/79421',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/79421/loki_2019_5?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/Loki-5/digital-comic/53004?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'reader',
            'url':
              'http://marvel.com/digitalcomics/view.htm?iid=53004&utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'inAppLink',
            'url':
              'https://applink.marvel.com/issue/53004?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/27739',
          'name': 'Loki (2019)',
        },
        'variants': [
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/82477',
            'name': 'Loki (2019) #5 (Variant)',
          },
        ],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2019-11-20T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2019-10-28T00:00:00-0400',
          },
          {
            'type': 'unlimitedDate',
            'date': '2020-05-25T00:00:00-0400',
          },
          {
            'type': 'digitalPurchaseDate',
            'date': '2019-11-20T00:00:00-0500',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 3.99,
          },
          {
            'type': 'digitalPurchasePrice',
            'price': 3.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/60/5dc98aa134d91',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/60/5dc98aa134d91',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 6,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/79421/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/10172',
              'name': 'Vc Clayton Cowles',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13021',
              'name': 'David Curiel',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13270',
              'name': 'Daniel Kibblesmith',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/7964',
              'name': 'Andy Macdonald',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12376',
              'name': 'Wilson Moss',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13585',
              'name': 'Ozgur Yildirim',
              'role': 'penciler (cover)',
            },
          ],
          'returned': 6,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/79421/characters',
          'items': [],
          'returned': 0,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/79421/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/175872',
              'name': 'cover from Loki (2019) #5',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/175873',
              'name': 'story from Loki (2019) #5',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/79421/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 82477,
        'digitalId': 0,
        'title': 'Loki (2019) #5 (Variant)',
        'issueNumber': 5,
        'variantDescription': 'Variant',
        'description': null,
        'modified': '2019-11-14T10:14:43-0500',
        'isbn': '',
        'upc': '75960609514800521',
        'diamondCode': 'SEP190904',
        'ean': '',
        'issn': '2374-6483',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/82477',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/82477/loki_2019_5_variant/variant?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/27739',
          'name': 'Loki (2019)',
        },
        'variants': [
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/79421',
            'name': 'Loki (2019) #5',
          },
        ],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2019-11-20T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2019-10-28T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 3.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/a/00/5dc9897112fec',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/a/00/5dc9897112fec',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/82477/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/10172',
              'name': 'Vc Clayton Cowles',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13021',
              'name': 'David Curiel',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13270',
              'name': 'Daniel Kibblesmith',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/7964',
              'name': 'Andy Macdonald',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/935',
              'name': 'Todd Nauck',
              'role': 'inker (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13127',
              'name': 'Emily Newcomen',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12991',
              'name': 'Rachelle Rosenberg',
              'role': 'colorist (cover)',
            },
          ],
          'returned': 7,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/82477/characters',
          'items': [],
          'returned': 0,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/82477/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/182712',
              'name': 'cover from Loki (2019) #5 (VARIANT)',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/182713',
              'name': 'story from Loki (2019) #5 (VARIANT)',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/82477/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 78319,
        'digitalId': 0,
        'title': 'Loki (2019) #4 (Variant)',
        'issueNumber': 4,
        'variantDescription': 'Variant',
        'description': null,
        'modified': '2019-10-03T13:47:05-0400',
        'isbn': '',
        'upc': '75960609514800421',
        'diamondCode': 'AUG191060',
        'ean': '',
        'issn': '2374-6483',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/78319',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/78319/loki_2019_4_variant/variant?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/27739',
          'name': 'Loki (2019)',
        },
        'variants': [
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/79420',
            'name': 'Loki (2019) #4',
          },
        ],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2019-10-09T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2019-09-16T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 3.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/6/40/5d924e6c245b0',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/6/40/5d924e6c245b0',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 8,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/78319/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13069',
              'name': 'Jen Bartel',
              'role': 'painter (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/10172',
              'name': 'Vc Clayton Cowles',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13021',
              'name': 'David Curiel',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/3708',
              'name': 'Carlos Lopez',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12998',
              'name': 'Jan Bazaldua - Inactive',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/447',
              'name': 'Victor Olazaba',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13270',
              'name': 'Daniel Kibblesmith',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13127',
              'name': 'Emily Newcomen',
              'role': 'editor',
            },
          ],
          'returned': 8,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/78319/characters',
          'items': [],
          'returned': 0,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/78319/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/173650',
              'name': 'cover from Loki (2019) #4 (VARIANT)',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/173651',
              'name': 'story from Loki (2019) #4 (VARIANT)',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/78319/events',
          'items': [],
          'returned': 0,
        },
      },
    ],
  },
};

export default comicNameFilter;
