/* eslint-disable quote-props */
export const mockComicDetail = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '2bb06aa82f2b5c40f940cfa398dd7565d61cbb2e',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 1,
    'count': 1,
    'results': [
      {
        'id': 93571,
        'digitalId': 57477,
        'title': 'Venom and Carnage',
        'issueNumber': 1,
        'variantDescription': '',
        'description':
          'Venom. Carnage. An aerial battle in the sky that may leave only one standing! Karla Pacheco, Scott Hepburn, and Ian Herring present Venom/Carnage! "Viva Las Venom!"',
        'modified': '2021-09-21T15:58:40-0400',
        'isbn': '',
        'upc': '75960620116700111',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Digital Vertical Comic',
        'pageCount': 10,
        'textObjects': [
          {
            'type': 'issue_solicit_text',
            'language': 'en-us',
            'text':
              'Venom. Carnage. An aerial battle in the sky that may leave only one standing! Karla Pacheco, Scott Hepburn, and Ian Herring present Venom/Carnage! "Viva Las Venom!"',
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/93571',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/93571/venomcarnage_infinity_comic_2021_1?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'reader',
            'url':
              'http://marvel.com/digitalcomics/view.htm?iid=57477&utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/31974',
          'name': 'Venom/Carnage Infinity Comic (2021 - Present)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2021-09-22T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2021-08-23T00:00:00-0400',
          },
          {
            'type': 'unlimitedDate',
            'date': '2021-09-22T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 0,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/e0/61438e24abeca',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/e0/61438e24abeca',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 5,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93571/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12298',
              'name': 'Scott Hepburn',
              'role': 'penciller (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12996',
              'name': 'Ian Herring',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12457',
              'name': 'Edward Devin Lewis',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13125',
              'name': 'Karla Pacheco',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12974',
              'name': 'Vc Joe Sabino',
              'role': 'letterer',
            },
          ],
          'returned': 5,
        },
        'characters': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93571/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009227',
              'name': 'Carnage',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010787',
              'name': 'Eddie Brock',
            },
          ],
          'returned': 2,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93571/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/208257',
              'name': 'cover from Venom/Carnage Vertical Comic (2021) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/208258',
              'name': 'story from Venom/Carnage Vertical Comic (2021) #1',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93571/events',
          'items': [],
          'returned': 0,
        },
      },
    ],
  },
};

export const mockComicCharacters = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '88d426f2ff4e814b6966f4325f15d7d482991250',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 2,
    'count': 2,
    'results': [
      {
        'id': 1009227,
        'name': 'Carnage',
        'description': '',
        'modified': '2016-02-18T12:28:26-0500',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/6/50/535fee423f170',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009227',
        'comics': {
          'available': 142,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009227/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76012',
              'name': 'Absolute Carnage (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76014',
              'name': 'Absolute Carnage (2019) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76015',
              'name': 'Absolute Carnage (2019) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76016',
              'name': 'Absolute Carnage (2019) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/78960',
              'name': 'Absolute Carnage (2019) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76013',
              'name': 'Absolute Carnage (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77396',
              'name': 'Absolute Carnage Vs. Deadpool (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77397',
              'name': 'Absolute Carnage Vs. Deadpool (2019) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77398',
              'name': 'Absolute Carnage Vs. Deadpool (2019) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77362',
              'name': 'Absolute Carnage Vs. Deadpool (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77060',
              'name': 'Absolute Carnage: Immortal Hulk (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/83394',
              'name': 'Absolute Carnage: Immortal Hulk And Other Tales  (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77061',
              'name': 'Absolute Carnage: Lethal Protectors (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77062',
              'name': 'Absolute Carnage: Lethal Protectors (2019) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77096',
              'name': 'Absolute Carnage: Lethal Protectors (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77064',
              'name': 'Absolute Carnage: Miles Morales (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77065',
              'name': 'Absolute Carnage: Miles Morales (2019) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77097',
              'name': 'Absolute Carnage: Miles Morales (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77067',
              'name': 'Absolute Carnage: Scream (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/77069',
              'name': 'Absolute Carnage: Scream (2019) #3',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 71,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009227/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27272',
              'name': 'Absolute Carnage (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27273',
              'name': 'Absolute Carnage (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27714',
              'name': 'Absolute Carnage Vs. Deadpool (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27720',
              'name': 'Absolute Carnage Vs. Deadpool (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27632',
              'name': 'Absolute Carnage: Immortal Hulk (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/29495',
              'name': 'Absolute Carnage: Immortal Hulk And Other Tales  (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27633',
              'name': 'Absolute Carnage: Lethal Protectors (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27646',
              'name': 'Absolute Carnage: Lethal Protectors (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27634',
              'name': 'Absolute Carnage: Miles Morales (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27647',
              'name': 'Absolute Carnage: Miles Morales (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27635',
              'name': 'Absolute Carnage: Scream (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27730',
              'name': 'Absolute Carnage: Symbiote Of Vengeance (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/29150',
              'name': 'Absolute Carnage: Weapon Plus (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1126',
              'name': 'Amazing Spider-Girl (2006 - 2009)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3101',
              'name': 'AMAZING SPIDER-GIRL VOL. 2: COMES THE CARNAGE! TPB (2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24411',
              'name': 'Amazing Spider-Man: Worldwide Vol. 9 (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/11942',
              'name': 'Carnage (2010 - 2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20720',
              'name': 'Carnage (2015 - 2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22112',
              'name': 'CARNAGE VOL. 3: WHAT DWELLS BENEATH TPB (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/14984',
              'name': 'Carnage, U.S.A. (2011 - 2012)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 153,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009227/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/5990',
              'name': 'Cover #5990',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14138',
              'name': 'Amazing Spider-Man (1963) #361',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14140',
              'name': 'Amazing Spider-Man (1963) #362',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14142',
              'name': 'Amazing Spider-Man (1963) #363',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14187',
              'name': 'Amazing Spider-Man (1963) #378',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14189',
              'name': 'Amazing Spider-Man (1963) #379',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14194',
              'name': 'Amazing Spider-Man (1963) #380',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14242',
              'name': 'Amazing Spider-Man (1963) #394',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14248',
              'name': 'Carnage',
              'type': 'profile',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14296',
              'name': 'Amazing Spider-Man (1963) #430',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14297',
              'name': 'Amazing Spider-Man (1963) #431',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25562',
              'name': 'Venom Triumphant!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25569',
              'name': 'When Carnage comes calling!',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/29779',
              'name': 'Maximum Carnage, Part 5 of 14',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/29782',
              'name': 'Maximum Carnage, Part 9 of 14',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/29785',
              'name': 'Maximum Carnage, Part 13 of 14',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/29876',
              'name': 'Peter Parker, The Spectacular Spider-Man (1976) #223',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/29914',
              'name': 'Web of Carnage, Part 4 of 4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/32606',
              'name': 'Carnage 2 of 4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/51299',
              'name': 'X-Men/Spider-Man (2008) #3',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009227/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/320',
              'name': 'Axis',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/151',
              'name': 'Maximum Carnage',
            },
          ],
          'returned': 2,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1009227/carnage?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Carnage_(Cletus_Kasady)?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1009227/carnage?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1010787,
        'name': 'Eddie Brock',
        'description': '',
        'modified': '2021-08-03T22:59:17-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/80/4de932f1a298a',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010787',
        'comics': {
          'available': 193,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010787/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76012',
              'name': 'Absolute Carnage (2019) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76014',
              'name': 'Absolute Carnage (2019) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76016',
              'name': 'Absolute Carnage (2019) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/78960',
              'name': 'Absolute Carnage (2019) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/6791',
              'name': 'The Amazing Spider-Man (1963) #378',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/67658',
              'name': 'Amazing Spider-Man Epic Collection: Venom (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/21798',
              'name': 'Amazing Spider-Man Family (2008) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/23180',
              'name': 'Amazing Spider-Man: Extra! (2008) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65261',
              'name': 'Amazing Spider-Man: Venom Inc. Alpha (2017) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65895',
              'name': 'Amazing Spider-Man: Venom Inc. Omega (2018) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/67091',
              'name': 'Avengers: Deathtrap - The Vault (1991) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65123',
              'name': 'Edge Of Venomverse (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/95144',
              'name': 'Free Comic Book Day: Spider-Man/Venom (2021) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65016',
              'name': 'Incredible Hulk Vs. Venom (1994) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/85649',
              'name': 'King in Black (2020) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/85650',
              'name': 'King in Black (2020) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/85652',
              'name': 'King in Black (2020) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/85653',
              'name': 'King in Black (2020) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/99137',
              'name': 'Venom Infinity Comic Primer (2021) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/71956',
              'name': 'Marvel Super Hero Adventures: Captain Marvel - Mealtime Mayhem (2018) #1',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 77,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010787/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27272',
              'name': 'Absolute Carnage (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/454',
              'name': 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24404',
              'name': 'Amazing Spider-Man Epic Collection: Venom (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/5376',
              'name': 'Amazing Spider-Man Family (2008 - 2009)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/6604',
              'name': 'Amazing Spider-Man: Extra! (2008 - 2009)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23769',
              'name': 'Amazing Spider-Man: Venom Inc. Alpha (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23906',
              'name': 'Amazing Spider-Man: Venom Inc. Omega (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24257',
              'name': 'Avengers: Deathtrap - The Vault (1991)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27311',
              'name': 'Edge Of Venomverse (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/32417',
              'name': 'Free Comic Book Day: Spider-Man/Venom (2021)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23658',
              'name': 'Incredible Hulk Vs. Venom (1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/30150',
              'name': 'King in Black (2020 - 2021)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/26234',
              'name': 'Marvel Super Hero Adventures: Captain Marvel - Mealtime Mayhem (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27073',
              'name': 'Marvel Tales: Venom (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/722',
              'name': 'Sensational Spider-Man (2006 - 2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27806',
              'name': 'Spider-Man & Venom: Double Trouble (2019 - 2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23705',
              'name': 'Spider-Man 2099 Meets Spider-Man (1995)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/192',
              'name': 'SPIDER-MAN LEGENDS VOL. 2: TODD MCFARLANE BOOK 2 TPB (2003)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24352',
              'name': 'Spider-Man Vs. Venom Omnibus (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3133',
              'name': 'Spider-Man, Peter Parker: Back in Black (2007)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 192,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010787/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14000',
              'name': 'Amazing Spider-Man (1963) #300',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14030',
              'name': 'Amazing Spider-Man (1963) #314',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14032',
              'name': 'Amazing Spider-Man (1963) #315',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14034',
              'name': 'Amazing Spider-Man (1963) #316',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14036',
              'name': 'Amazing Spider-Man (1963) #317',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14066',
              'name': 'Amazing Spider-Man (1963) #330',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14068',
              'name': 'Amazing Spider-Man (1963) #331',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14070',
              'name': 'Amazing Spider-Man (1963) #332',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14072',
              'name': 'Amazing Spider-Man (1963) #333',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14096',
              'name': 'Amazing Spider-Man (1963) #344',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14098',
              'name': 'Amazing Spider-Man (1963) #345',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14140',
              'name': 'Amazing Spider-Man (1963) #362',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14142',
              'name': 'Amazing Spider-Man (1963) #363',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14174',
              'name': 'Amazing Spider-Man (1963) #374',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14176',
              'name': 'Amazing Spider-Man (1963) #375',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14187',
              'name': 'Amazing Spider-Man (1963) #378',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/32299',
              'name': 'SENSATIONAL SPIDER-MAN (2006) #38',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/40330',
              'name': 'interior to Revenge of the Green Goblin #1',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/47802',
              'name': '1 of 5 - 5XLS',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/47905',
              'name': 'AMAZING SPIDER-MAN FAMILY (2008)#1',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010787/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/151',
              'name': 'Maximum Carnage',
            },
          ],
          'returned': 1,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/characters/2843/puff_adder?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Brock,_Eddie?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1010787/eddie_brock?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
    ],
  },
};

export const mockComicStories = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '3431606a1ad21c3a921b841d69fab304174d20f3',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 2,
    'count': 2,
    'results': [
      {
        'id': 208257,
        'title': 'cover from Venom/Carnage',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/208257',
        'type': 'cover',
        'modified': '2021-09-21T12:31:18-0400',
        'thumbnail': null,
        'creators': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208257/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12298',
              'name': 'Scott Hepburn',
              'role': 'penciller (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12457',
              'name': 'Edward Devin Lewis',
              'role': 'editor',
            },
          ],
          'returned': 2,
        },
        'characters': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208257/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009227',
              'name': 'Carnage',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010787',
              'name': 'Eddie Brock',
            },
          ],
          'returned': 2,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208257/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/31974',
              'name': 'Venom/Carnage Infinity Comic (2021 - Present)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208257/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/93571',
              'name': 'Venom/Carnage Infinity Comic (2021) #1',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208257/events',
          'items': [],
          'returned': 0,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/93571',
          'name': 'Venom/Carnage Infinity Comic (2021) #1',
        },
      },
      {
        'id': 208258,
        'title': 'story from Venom/Carnage Vertical Comic (2021) #1',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/208258',
        'type': 'story',
        'modified': '2021-09-21T12:30:52-0400',
        'thumbnail': null,
        'creators': {
          'available': 5,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208258/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12298',
              'name': 'Scott Hepburn',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12996',
              'name': 'Ian Herring',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12457',
              'name': 'Edward Devin Lewis',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13125',
              'name': 'Karla Pacheco',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12974',
              'name': 'Vc Joe Sabino',
              'role': 'letterer',
            },
          ],
          'returned': 5,
        },
        'characters': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208258/characters',
          'items': [],
          'returned': 0,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208258/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/31974',
              'name': 'Venom/Carnage Infinity Comic (2021 - Present)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208258/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/93571',
              'name': 'Venom/Carnage Infinity Comic (2021) #1',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/208258/events',
          'items': [],
          'returned': 0,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/93571',
          'name': 'Venom/Carnage Infinity Comic (2021) #1',
        },
      },
    ],
  },
};
