/* eslint-disable quote-props */
export const mockStoryDetail = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '4cef1c9010da1161baf4dd3e09c3718ebed42c3c',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 1,
    'count': 1,
    'results': [
      {
        'id': 25637,
        'title': 'Universe X (2000) #12',
        'description':
          'The Silver Surfer is joined by the Iron Avengers, the Sentinels, and Magneto, to battle the Absorbing Man! Meanwhile, Peter Parker, Venom, and the New York heroes do what they can to stave off the end of the world.',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25637',
        'type': 'cover',
        'modified': '2018-10-25T16:10:43-0400',
        'thumbnail': null,
        'creators': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/25637/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4174',
              'name': 'Mike Marts',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/63',
              'name': 'Alex Ross',
              'role': 'penciller (cover)',
            },
          ],
          'returned': 2,
        },
        'characters': {
          'available': 16,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/25637/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010699',
              'name': 'Aaron Stack',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009148',
              'name': 'Absorbing Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010354',
              'name': 'Adam Warlock',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009184',
              'name': 'Black Bolt',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009224',
              'name': 'Captain Marvel (Mar-Vell)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010813',
              'name': 'Celestials',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1012512',
              'name': 'Gargoyle (Isaac Christians)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1012923',
              'name': 'Immortus',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009384',
              'name': 'Kang',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009407',
              'name': 'Loki',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009417',
              'name': 'Magneto',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009440',
              'name': 'Mephisto',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009491',
              'name': 'Peter Parker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009592',
              'name': 'Silver Surfer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010901',
              'name': 'Stephen Strange',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009631',
              'name': 'Sue Storm',
            },
          ],
          'returned': 16,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/25637/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2085',
              'name': 'Universe X (2000 - 2001)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/25637/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11905',
              'name': 'Universe X (2000) #12',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/25637/events',
          'items': [],
          'returned': 0,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11905',
          'name': 'Universe X (2000) #12',
        },
      },
    ],
  },
};

export const mockStoryCharacters = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '1c7e7143d4d5ecbd26f4643454cd8595ee22cf61',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 16,
    'count': 5,
    'results': [
      {
        'id': 1010699,
        'name': 'Aaron Stack',
        'description': '',
        'modified': '1969-12-31T19:00:00-0500',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010699',
        'comics': {
          'available': 14,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010699/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40776',
              'name': 'Dark Avengers (2012) #177',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40773',
              'name': 'Dark Avengers (2012) #179',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40774',
              'name': 'Dark Avengers (2012) #180',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40778',
              'name': 'Dark Avengers (2012) #181',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40787',
              'name': 'Dark Avengers (2012) #182',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40786',
              'name': 'Dark Avengers (2012) #183',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38073',
              'name': 'Hulk (2008) #43',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11910',
              'name': 'Universe X (2000) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11911',
              'name': 'Universe X (2000) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11912',
              'name': 'Universe X (2000) #8',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11913',
              'name': 'Universe X (2000) #9',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11903',
              'name': 'Universe X (2000) #10',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11904',
              'name': 'Universe X (2000) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11905',
              'name': 'Universe X (2000) #12',
            },
          ],
          'returned': 14,
        },
        'series': {
          'available': 3,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010699/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/789',
              'name': 'Dark Avengers (2012 - 2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3374',
              'name': 'Hulk (2008 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2085',
              'name': 'Universe X (2000 - 2001)',
            },
          ],
          'returned': 3,
        },
        'stories': {
          'available': 27,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010699/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25634',
              'name': 'Universe X (2000) #10',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25635',
              'name': 'Interior #25635',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25637',
              'name': 'Universe X (2000) #12',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25638',
              'name': 'Interior #25638',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25647',
              'name': 'Universe X (2000) #6',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25648',
              'name': 'Interior #25648',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25649',
              'name': 'Universe X (2000) #7',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25650',
              'name': 'Interior #25650',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25651',
              'name': 'Universe X (2000) #8',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25652',
              'name': 'Interior #25652',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25653',
              'name': 'Universe X (2000) #9',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25654',
              'name': 'Interior #25654',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/67100',
              'name': 'Universe X (2000) #11',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/89190',
              'name': 'Hulk (2008) #43',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/90002',
              'name': 'Interior #90002',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92370',
              'name': 'Dark Avengers (2012) #179',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92371',
              'name': 'Interior #92371',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92372',
              'name': 'Dark Avengers (2012) #180',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92373',
              'name': 'Interior #92373',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92376',
              'name': 'Dark Avengers (2012) #177',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010699/events',
          'items': [],
          'returned': 0,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/characters/2809/aaron_stack?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1010699/aaron_stack?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1009148,
        'name': 'Absorbing Man',
        'description': '',
        'modified': '2013-10-24T14:32:08-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/1/b0/5269678709fb7',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009148',
        'comics': {
          'available': 96,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009148/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/43507',
              'name': 'A+X (2012) #8',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/7045',
              'name': 'Avengers (1963) #183',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/7046',
              'name': 'Avengers (1963) #184',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/7142',
              'name': 'Avengers (1963) #270',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/36481',
              'name': 'Avengers Academy (2010) #16',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/36480',
              'name': 'Avengers Academy (2010) #17',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/36479',
              'name': 'Avengers Academy (2010) #18',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/36484',
              'name': 'Avengers Academy (2010) #19',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17776',
              'name': 'Avengers Annual (1967) #20',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/63662',
              'name': 'Black Bolt (2017) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/64278',
              'name': 'Black Bolt (2017) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66533',
              'name': 'Black Bolt (2017) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65327',
              'name': 'Black Bolt Vol. 1: Hard Time (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12783',
              'name': 'Captain America (1998) #24',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20427',
              'name': 'Dazzler (1981) #18',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20428',
              'name': 'Dazzler (1981) #19',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8499',
              'name': 'Deadpool (1997) #43',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/15541',
              'name': 'Fantastic Four (1998) #22',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/13151',
              'name': 'Fantastic Four (1961) #330',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/41433',
              'name': 'Fear Itself (2010) #2 (3rd Printing Variant)',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 48,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009148/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/16450',
              'name': 'A+X (2012 - 2014)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1991',
              'name': 'Avengers (1963 - 1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9086',
              'name': 'Avengers Academy (2010 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1988',
              'name': 'Avengers Annual (1967 - 1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23121',
              'name': 'Black Bolt (2017 - 2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23778',
              'name': 'Black Bolt Vol. 1: Hard Time (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1997',
              'name': 'Captain America (1998 - 2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3745',
              'name': 'Dazzler (1981 - 1986)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2005',
              'name': 'Deadpool (1997 - 2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2121',
              'name': 'Fantastic Four (1961 - 1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/421',
              'name': 'Fantastic Four (1998 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13691',
              'name': 'Fear Itself (2010 - 2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13857',
              'name': 'Fear Itself: Fellowship of Fear (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13827',
              'name': 'Fear Itself: The Worthy (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/31372',
              'name': 'Gamma Flight (2021)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20084',
              'name': 'Heroes for Hire (1997 - 1999)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/465',
              'name': 'Hulk (1999 - 2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20552',
              'name': 'Illuminati (2015 - 2016)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24278',
              'name': 'Immortal Hulk (2018 - Present)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24891',
              'name': 'Immortal Hulk Vol. 2: The Green Door (2019)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 109,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009148/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/4988',
              'name': '1 of 1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/7866',
              'name': 'Punisher War Journal (2006) #4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/10997',
              'name': 'Journey Into Mystery (1952) #114',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/10998',
              'name': 'The Stronger I Am, the Sooner I Die',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11000',
              'name': 'Journey Into Mystery (1952) #115',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11001',
              'name': 'The Vengeance of the Thunder God',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11022',
              'name': 'Journey Into Mystery (1952) #120',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11023',
              'name': 'With My Hammer In Hand',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11025',
              'name': 'Journey Into Mystery (1952) #121',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11026',
              'name': 'The Power!  The Passion!  The Pride!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11028',
              'name': 'Journey Into Mystery (1952) #122',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11029',
              'name': 'Where Mortals Fear To Tread!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11031',
              'name': 'Journey Into Mystery (1952) #123',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/11032',
              'name': 'While a Universe Trembles',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12951',
              'name': 'Fantastic Four (1961) #330',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12952',
              'name': 'Good Dreams!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14628',
              'name': 'Avengers (1963) #183',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14630',
              'name': 'Avengers (1963) #184',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/14823',
              'name': 'Avengers (1963) #270',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/16688',
              'name': 'Thor (1966) #206',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 4,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009148/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/238',
              'name': 'Civil War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/302',
              'name': 'Fear Itself',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/270',
              'name': 'Secret Wars',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/273',
              'name': 'Siege',
            },
          ],
          'returned': 4,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/characters/84/absorbing_man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Absorbing_Man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1009148/absorbing_man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1010354,
        'name': 'Adam Warlock',
        'description':
          'Adam Warlock is an artificially created human who was born in a cocoon at a scientific complex called The Beehive.',
        'modified': '2013-08-07T13:49:06-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/a/f0/5202887448860',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010354',
        'comics': {
          'available': 188,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010354/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/62151',
              'name': 'All-New Guardians of the Galaxy Vol. 3: Infinity Quest (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17271',
              'name': 'Annihilation: Conquest (2007) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17405',
              'name': 'Annihilation: Conquest (2007) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17645',
              'name': 'Annihilation: Conquest (2007) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20686',
              'name': 'Annihilation: Conquest (2007) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20885',
              'name': 'Annihilation: Conquest (2007) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/21016',
              'name': 'Annihilation: Conquest (2007) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12412',
              'name': 'Avengers Forever (1998) #9',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/1033',
              'name': 'Avengers Legends Vol. I: Avengers Forever (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20731',
              'name': 'CLANDESTINE CLASSIC PREMIERE HC (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20187',
              'name': 'Doctor Strange, Sorcerer Supreme (1988) #27',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20193',
              'name': 'Doctor Strange, Sorcerer Supreme (1988) #32',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/20197',
              'name': 'Doctor Strange, Sorcerer Supreme (1988) #36',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8552',
              'name': 'Earth X (1999) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8550',
              'name': 'Earth X (1999) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4241',
              'name': 'EARTH X TPB [NEW PRINTING] (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12975',
              'name': 'Fantastic Four (1961) #172',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/13195',
              'name': 'Fantastic Four (1961) #370',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/25305',
              'name': 'Guardians of the Galaxy (2008) #17',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65547',
              'name': 'All-New Guardians of the Galaxy (2017) #150',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 82,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010354/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23058',
              'name': 'All-New Guardians of the Galaxy (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22778',
              'name': 'All-New Guardians of the Galaxy Vol. 3: Infinity Quest (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3061',
              'name': 'Annihilation: Conquest (2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2111',
              'name': 'Avengers Forever (1998 - 1999)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/93',
              'name': 'Avengers Legends Vol. I: Avengers Forever (2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3874',
              'name': 'CLANDESTINE CLASSIC PREMIERE HC (2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3741',
              'name': 'Doctor Strange, Sorcerer Supreme (1988 - 1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/378',
              'name': 'Earth X (1999 - 2000)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1806',
              'name': 'EARTH X TPB [NEW PRINTING] (2006)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2121',
              'name': 'Fantastic Four (1961 - 1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/4885',
              'name': 'Guardians of the Galaxy (2008 - 2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27554',
              'name': 'Guardians Of The Galaxy Annual (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/26496',
              'name': 'Guardians Of The Galaxy Vol. 2: Faithless (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22422',
              'name': 'GUARDIANS OF THE GALAXY: ROAD TO ANNIHILATION VOL. 2 TPB (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2021',
              'name': 'Incredible Hulk (1962 - 1999)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2983',
              'name': 'Incredible Hulk Annual (1976 - 1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/26307',
              'name': 'Infinity By Starlin & Hickman (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24050',
              'name': 'Infinity Countdown (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24300',
              'name': 'Infinity Countdown Prime (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23991',
              'name': 'Infinity Countdown: Adam Warlock (2018)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 217,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010354/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1412',
              'name': 'Cover #1412',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1602',
              'name': 'Cover #1602',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1800',
              'name': 'Cover #1800',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1842',
              'name': 'Cover #1842',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3758',
              'name': 'WARLOCK (2004) #3',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3760',
              'name': 'WARLOCK (2004) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3762',
              'name': 'WARLOCK (2004) #2',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3764',
              'name': 'WARLOCK (2004) #4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12568',
              'name': 'Fantastic Four (1961) #172',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12569',
              'name': 'Cry, the Bedeviled Planet!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/13121',
              'name': 'Forever Evil',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18500',
              'name': 'Incredible Hulk (1962) #177',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18501',
              'name': 'Peril of the Paired Planets',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18503',
              'name': 'Triumph On Terra-Two',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19847',
              'name': 'Cover #19847',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19848',
              'name': 'Performance',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19859',
              'name': 'Days of Future Present Part 4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19860',
              'name': 'You Must Remember This',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19883',
              'name': 'The Adventures of Lockheed the Space Dragon and His Pet Girl, Kitty',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19884',
              'name': 'The Saga of Storm: Goddess of Thunder',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 8,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010354/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/293',
              'name': 'Annihilation: Conquest',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/233',
              'name': 'Atlantis Attacks',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/235',
              'name': 'Blood and Thunder',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/240',
              'name': 'Days of Future Present',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/29',
              'name': 'Infinity War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/263',
              'name': 'Mutant Massacre',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/271',
              'name': 'Secret Wars II',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/280',
              'name': 'X-Tinction Agenda',
            },
          ],
          'returned': 8,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1010354/adam_warlock?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Warlock,_Adam?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1010354/adam_warlock?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1009184,
        'name': 'Black Bolt',
        'description': '',
        'modified': '2013-10-24T14:39:01-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/1/20/52696929dc721',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009184',
        'comics': {
          'available': 161,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009184/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/7349',
              'name': 'Avengers (1963) #95',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/6983',
              'name': 'Avengers (1963) #127',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/7050',
              'name': 'Avengers (1963) #188',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/7088',
              'name': 'Avengers (1963) #221',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/37287',
              'name': 'Avengers Academy (2010) #2 (2ND PRINTING VARIANT)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/63431',
              'name': 'Black Bolt (2017) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/63166',
              'name': 'Black Bolt (2017) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/63662',
              'name': 'Black Bolt (2017) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/64278',
              'name': 'Black Bolt (2017) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/64607',
              'name': 'Black Bolt (2017) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/64884',
              'name': 'Black Bolt (2017) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65058',
              'name': 'Black Bolt (2017) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65267',
              'name': 'Black Bolt (2017) #8',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65436',
              'name': 'Black Bolt (2017) #9',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66268',
              'name': 'Black Bolt (2017) #10',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66533',
              'name': 'Black Bolt (2017) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66898',
              'name': 'Black Bolt (2017) #12',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/78057',
              'name': 'Black Bolt (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65327',
              'name': 'Black Bolt Vol. 1: Hard Time (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/67314',
              'name': 'Black Bolt Vol. 2: Home Free (Trade Paperback)',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 71,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009184/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1991',
              'name': 'Avengers (1963 - 1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9086',
              'name': 'Avengers Academy (2010 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23121',
              'name': 'Black Bolt (2017 - 2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27898',
              'name': 'Black Bolt (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23778',
              'name': 'Black Bolt Vol. 1: Hard Time (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24326',
              'name': 'Black Bolt Vol. 2: Home Free (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/784',
              'name': 'Black Panther (2005 - 2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2226',
              'name': 'Black Panther: Civil War (2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2000',
              'name': 'Captain Marvel (1968 - 1979)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2002',
              'name': 'Daredevil (1964 - 1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22313',
              'name': 'Death of X (2016)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/378',
              'name': 'Earth X (1999 - 2000)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1806',
              'name': 'EARTH X TPB [NEW PRINTING] (2006)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1263',
              'name': 'Essential Fantastic Four Vol. 4 (2005)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/421',
              'name': 'Fantastic Four (1998 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2123',
              'name': 'Fantastic Four (1996 - 1997)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2121',
              'name': 'Fantastic Four (1961 - 1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2012',
              'name': 'Fantastic Four Annual (1963 - 1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/341',
              'name': 'FANTASTIC FOUR VISIONARIES: JOHN BYRNE VOL. 2 TPB (2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2445',
              'name': 'Fantastic Four/Inhumans (2007)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 174,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009184/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3469',
              'name': '4 of 4 - Sentry',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3846',
              'name': '5 of 5 - Bride of the Panther',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3848',
              'name': 'Cover #3848',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3852',
              'name': 'Cover #3852',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/4181',
              'name': 'Cover #4181',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/5331',
              'name': 'Ultimate Fantastic Four Annual (2005) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/5332',
              'name': '1 of 1 - Inhumans',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/6232',
              'name': '1 of 6 - Silent War',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/6238',
              'name': '1 of 5 - 5XLS',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/7896',
              'name': '2 of 6 - Silent War',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/7906',
              'name': '2 of 5 - 5 XLS',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/8349',
              'name': '3 of 6 - Silent War',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/8351',
              'name': '4 of 6 - Silent War',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/8689',
              'name': '3 of 5 - 5XLS - Secret Wars',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12759',
              'name': 'Fantastic Four (1961) #248',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12760',
              'name': 'Nightmare!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12898',
              'name': 'The Marvel Rage!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/13145',
              'name': 'Suddenly ... The Secret Defenders',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/13151',
              'name': "It's Always Darkest Before the ... DOOM!",
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/13258',
              'name': 'Fantastic Four (1961) #391',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 10,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009184/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/238',
              'name': 'Civil War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/315',
              'name': 'Infinity',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/317',
              'name': 'Inhumanity',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/311',
              'name': 'Marvel NOW!',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/319',
              'name': 'Original Sin',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/295',
              'name': 'Realm of Kings',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/269',
              'name': 'Secret Invasion',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/323',
              'name': 'Secret Wars (2015)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/276',
              'name': 'War of Kings',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/277',
              'name': 'World War Hulk',
            },
          ],
          'returned': 10,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1009184/black_bolt?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Black_Bolt?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1009184/black_bolt?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1009224,
        'name': 'Captain Marvel (Mar-Vell)',
        'description': '',
        'modified': '2013-10-17T14:53:19-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/f/60/526032048d1a1',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009224',
        'comics': {
          'available': 198,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009224/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/5844',
              'name': 'Avengers Assemble Vol. 4 (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66755',
              'name': 'Avengers Epic Collection: The Avengers/Defenders War (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/6946',
              'name': 'Avengers Forever (1998) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12405',
              'name': 'Avengers Forever (1998) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12406',
              'name': 'Avengers Forever (1998) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12407',
              'name': 'Avengers Forever (1998) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12408',
              'name': 'Avengers Forever (1998) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12409',
              'name': 'Avengers Forever (1998) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12410',
              'name': 'Avengers Forever (1998) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12411',
              'name': 'Avengers Forever (1998) #8',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12412',
              'name': 'Avengers Forever (1998) #9',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12402',
              'name': 'Avengers Forever (1998) #10',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12403',
              'name': 'Avengers Forever (1998) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12404',
              'name': 'Avengers Forever (1998) #12',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/1033',
              'name': 'Avengers Legends Vol. I: Avengers Forever (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/60897',
              'name': 'AVENGERS: THE COMPLETE CELESTIAL MADONNA SAGA TPB (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12774',
              'name': 'Captain America (1998) #16',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17167',
              'name': 'Captain Marvel (1968) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17252',
              'name': 'Captain Marvel (2008) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17387',
              'name': 'Captain Marvel (2008) #2',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 73,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009224/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1816',
              'name': 'Avengers Assemble Vol. 4 (2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24152',
              'name': 'Avengers Epic Collection: The Avengers/Defenders War (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2111',
              'name': 'Avengers Forever (1998 - 1999)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/93',
              'name': 'Avengers Legends Vol. I: Avengers Forever (2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22468',
              'name': 'AVENGERS: THE COMPLETE CELESTIAL MADONNA SAGA TPB (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1997',
              'name': 'Captain America (1998 - 2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3041',
              'name': 'Captain Marvel (2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/6258',
              'name': 'Captain Marvel (2002 - 2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/690',
              'name': 'Captain Marvel (2000 - 2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2000',
              'name': 'Captain Marvel (1968 - 1979)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/175',
              'name': 'Captain Marvel Vol. 1: Nothing to Lose (2003)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/105',
              'name': 'Captain Marvel Vol. I (1999)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/259',
              'name': 'Captain Marvel Vol. II: Coven (2003)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/11854',
              'name': 'Chaos War (2010 - 2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13527',
              'name': 'Chaos War: Avengers (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13257',
              'name': 'Chaos War: Dead Avengers (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2002',
              'name': 'Daredevil (1964 - 1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2005',
              'name': 'Deadpool (1997 - 2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3743',
              'name': 'Defenders (1972 - 1986)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3741',
              'name': 'Doctor Strange, Sorcerer Supreme (1988 - 1996)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 291,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009224/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3546',
              'name': 'Ultimate Extinction (2006) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3547',
              'name': '1 of 5 - 5XLS',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3548',
              'name': 'Ultimate Extinction (2006) #2',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3549',
              'name': '2 of 5 - 5XLS',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3550',
              'name': 'Ultimate Extinction (2006) #3',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3551',
              'name': '3 of 5 - 5XLS',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3552',
              'name': 'Ultimate Extinction (2006) #4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3553',
              'name': '4 of 5 - 5XLS',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3554',
              'name': 'Ultimate Extinction (2006) #5',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/3555',
              'name': '5 of 5 - 5XLS',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/12543',
              'name': 'Captain Marvel in The Big Bang',
              'type': 'ad',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/15297',
              'name': 'Summons From the Stars!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/15839',
              'name': 'Captain Marvel Meets the Dreadnought',
              'type': 'ad',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/16998',
              'name': 'Ragnarok and Roll',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/17001',
              'name': 'THOR (1966) #352',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/17002',
              'name': 'Ragnarok and Ruin',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18231',
              'name': 'Cover #18231',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18232',
              'name': 'Die, Traitor',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18233',
              'name': 'Cover #18233',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/18234',
              'name': 'Rebirth!',
              'type': 'interiorStory',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 6,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009224/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/296',
              'name': 'Chaos War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/37',
              'name': 'Maximum Security',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/269',
              'name': 'Secret Invasion',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/270',
              'name': 'Secret Wars',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/271',
              'name': 'Secret Wars II',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/294',
              'name': 'The Thanos Imperative',
            },
          ],
          'returned': 6,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1009224/captain_marvel_mar-vell?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Captain_Marvel_(Mar-Vell)?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1009224/captain_marvel_mar-vell?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
    ],
  },
};

export const mockStoryComics = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '03e5e164b2d47d19a0117a8a63c95188e47e1a1a',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 1,
    'count': 1,
    'results': [
      {
        'id': 11905,
        'digitalId': 3631,
        'title': 'Universe X (2000) #12',
        'issueNumber': 12,
        'variantDescription': '',
        'description':
          'The Silver Surfer is joined by the Iron Avengers, the Sentinels, and Magneto, to battle the Absorbing Man! Meanwhile, Peter Parker, Venom, and the New York heroes do what they can to stave off the end of the world',
        'modified': '2017-01-11T15:11:27-0500',
        'isbn': '',
        'upc': '',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Comic',
        'pageCount': 0,
        'textObjects': [
          {
            'type': 'issue_preview_text',
            'language': 'en-us',
            'text':
              'The Silver Surfer is joined by the Iron Avengers, the Sentinels, and Magneto, to battle the Absorbing Man! Meanwhile, Peter Parker, Venom, and the New York heroes do what they can to stave off the end of the world',
          },
        ],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/11905',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/11905/universe_x_2000_12?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/Universe-X-12/digital-comic/3631?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'reader',
            'url':
              'http://marvel.com/digitalcomics/view.htm?iid=3631&utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'inAppLink',
            'url':
              'https://applink.marvel.com/issue/3631?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/2085',
          'name': 'Universe X (2000 - 2001)',
        },
        'variants': [],
        'collections': [
          {
            'resourceURI': 'http://gateway.marvel.com/v1/public/comics/6009',
            'name': 'UNIVERSE X VOL. 2 TPB [NEW PRINTING] (Trade Paperback)',
          },
        ],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2001-09-01T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '-0001-11-30T00:00:00-0500',
          },
          {
            'type': 'unlimitedDate',
            'date': '2010-01-28T00:00:00-0500',
          },
          {
            'type': 'digitalPurchaseDate',
            'date': '2011-08-02T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 0,
          },
          {
            'type': 'digitalPurchasePrice',
            'price': 1.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/5/90/5bd2225ab1635',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/5/90/5bd2225ab1635',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/11905/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/128',
              'name': 'Doug Braithwaite',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/7843',
              'name': 'Todd Klein',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/267',
              'name': 'Jim Krueger',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4174',
              'name': 'Mike Marts',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/523',
              'name': 'Bill Reinhold',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/2121',
              'name': 'Robin Riggs',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/63',
              'name': 'Alex Ross',
              'role': 'penciller (cover)',
            },
          ],
          'returned': 7,
        },
        'characters': {
          'available': 16,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/11905/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010699',
              'name': 'Aaron Stack',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009148',
              'name': 'Absorbing Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010354',
              'name': 'Adam Warlock',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009184',
              'name': 'Black Bolt',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009224',
              'name': 'Captain Marvel (Mar-Vell)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010813',
              'name': 'Celestials',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1012512',
              'name': 'Gargoyle (Isaac Christians)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1012923',
              'name': 'Immortus',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009384',
              'name': 'Kang',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009407',
              'name': 'Loki',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009417',
              'name': 'Magneto',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009440',
              'name': 'Mephisto',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009491',
              'name': 'Peter Parker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009592',
              'name': 'Silver Surfer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010901',
              'name': 'Stephen Strange',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009631',
              'name': 'Sue Storm',
            },
          ],
          'returned': 16,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/11905/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25637',
              'name': 'Universe X (2000) #12',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/25638',
              'name': 'Interior #25638',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/11905/events',
          'items': [],
          'returned': 0,
        },
      },
    ],
  },
};
