/* eslint-disable quote-props */
export const mockCharacterDetail = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '108d02c2aa5b979229491aab853eb5b4293682b9',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 1,
    'count': 1,
    'results': [
      {
        'id': 1009368,
        'name': 'Iron Man',
        'description':
          'Wounded, captured and forced to build a weapon by his enemies, billionaire industrialist Tony Stark instead created an advanced suit of armor to save his life and escape captivity. Now with a new outlook on life, Tony uses his money and intelligence to make the world a safer, better place as Iron Man.',
        'modified': '2016-09-28T12:08:19-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
        'comics': {
          'available': 2589,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009368/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/43495',
              'name': 'A+X (2012) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/43506',
              'name': 'A+X (2012) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/22461',
              'name': 'Adam: Legend of the Blue Marvel (2008) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/22856',
              'name': 'Adam: Legend of the Blue Marvel (2008) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/23733',
              'name': 'Adam: Legend of the Blue Marvel (2008) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76359',
              'name': 'Aero (2019) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/76360',
              'name': 'Aero (2019) #12',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/30090',
              'name': 'Age of Heroes (2010) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/33566',
              'name': 'Age of Heroes (2010) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/30092',
              'name': 'Age of Heroes (2010) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/30093',
              'name': 'Age of Heroes (2010) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/67603',
              'name': 'Age of Innocence: The Rebirth of Iron Man (1996) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38524',
              'name': 'Age of X: Universe (2011) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38523',
              'name': 'Age of X: Universe (2011) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/21280',
              'name': 'All-New Iron Manual (2008) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/55363',
              'name': 'All-New, All-Different Avengers (2015) #10',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/55364',
              'name': 'All-New, All-Different Avengers (2015) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12653',
              'name': 'Alpha Flight (1983) #113',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12668',
              'name': 'Alpha Flight (1983) #127',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/55311',
              'name': 'The Amazing Spider-Man (2015) #13',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 635,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009368/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/16450',
              'name': 'A+X (2012 - 2014)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/6079',
              'name': 'Adam: Legend of the Blue Marvel (2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27392',
              'name': 'Aero (2019 - 2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9790',
              'name': 'Age of Heroes (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24380',
              'name': 'Age of Innocence: The Rebirth of Iron Man (1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13896',
              'name': 'Age of X: Universe (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/4897',
              'name': 'All-New Iron Manual (2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20443',
              'name': 'All-New, All-Different Avengers (2015 - 2016)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2116',
              'name': 'Alpha Flight (1983 - 1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/454',
              'name': 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2984',
              'name': 'Amazing Spider-Man Annual (1964 - 2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1489',
              'name': 'AMAZING SPIDER-MAN VOL. 10: NEW AVENGERS TPB (2005)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/318',
              'name': 'Amazing Spider-Man Vol. 6 (2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23446',
              'name': 'Amazing Spider-Man: Worldwide Vol. 2 (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/6056',
              'name': 'ANNIHILATION CLASSIC HC (2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/14818',
              'name': 'Annihilators: Earthfall (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/14779',
              'name': 'Art of Marvel Studios TPB Slipcase (2011 - Present)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9792',
              'name': 'Astonishing Spider-Man & Wolverine (2010 - 2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/6792',
              'name': 'Astonishing Tales (2009)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/6697',
              'name': 'Astonishing Tales: Iron Man 2020 Digital Comic (2009)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 3915,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009368/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/670',
              'name': 'X-MEN (2004) #186',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/892',
              'name': 'THOR (1998) #81',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/960',
              'name': '3 of ?',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/982',
              'name': 'Interior #982',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/984',
              'name': 'Interior #984',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/986',
              'name': 'Interior #986',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/988',
              'name': 'Interior #988',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/990',
              'name': 'Interior #990',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/992',
              'name': 'Interior #992',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/994',
              'name': 'Interior #994',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/996',
              'name': 'Interior #996',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/998',
              'name': 'Interior #998',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1000',
              'name': 'Interior #1000',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1002',
              'name':
                'AVENGERS DISASSEMBLED TIE-IN! Still reeling from recent traumas, Iron Man must face off against his evil doppelganger. Meanwhile',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1004',
              'name':
                '"THE SINGULARITY" CONCLUSION! PART 4 (OF 4) The battle rages on between Iron Man and his doppelganger, but only one of them can ',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1018',
              'name': 'Amazing Spider-Man (1999) #500',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1024',
              'name': 'Avengers (1998) #80',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1026',
              'name': 'Avengers (1998) #81',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1041',
              'name': 'Avengers (1998) #502',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1051',
              'name': 'Interior #1051',
              'type': 'interiorStory',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 31,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009368/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/116',
              'name': 'Acts of Vengeance!',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/303',
              'name': 'Age of X',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/231',
              'name': 'Armor Wars',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/233',
              'name': 'Atlantis Attacks',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/234',
              'name': 'Avengers Disassembled',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/310',
              'name': 'Avengers VS X-Men',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/296',
              'name': 'Chaos War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/238',
              'name': 'Civil War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/239',
              'name': 'Crossing',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/318',
              'name': 'Dark Reign',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/245',
              'name': 'Enemy of the State',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/249',
              'name': 'Fatal Attractions',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/302',
              'name': 'Fear Itself',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/251',
              'name': 'House of M',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/315',
              'name': 'Infinity',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/29',
              'name': 'Infinity War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/317',
              'name': 'Inhumanity',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/255',
              'name': 'Initiative',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/37',
              'name': 'Maximum Security',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/154',
              'name': 'Onslaught',
            },
          ],
          'returned': 20,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/characters/29/iron_man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Iron_Man_(Anthony_Stark)?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1009368/iron_man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
    ],
  },
};

export const mockCharacterComics = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': 'ab8f97d640c71be19235fd991fc1c56d5d1aaf5c',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 2589,
    'count': 5,
    'results': [
      {
        'id': 27238,
        'digitalId': 0,
        'title': 'Wolverine Saga (2009) #7',
        'issueNumber': 7,
        'variantDescription': '',
        'description': null,
        'modified': '-0001-11-30T00:00:00-0500',
        'isbn': '',
        'upc': '5960606814-00711',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/27238',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/27238/wolverine_saga_2009_7?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/8086',
          'name': 'Wolverine Saga (2009)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2029-12-31T00:00:00-0500',
          },
          {
            'type': 'focDate',
            'date': '2009-06-11T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 0,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          'extension': 'jpg',
        },
        'images': [],
        'creators': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/27238/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4430',
              'name': 'Jeff Youngquist',
              'role': 'editor',
            },
          ],
          'returned': 1,
        },
        'characters': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/27238/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009718',
              'name': 'Wolverine',
            },
          ],
          'returned': 2,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/27238/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/59792',
              'name': 'Cover #59792',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/59793',
              'name': 'Interior #59793',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/27238/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 95900,
        'digitalId': 0,
        'title': 'Death Of Doctor Strange: Avengers (2021) #1',
        'issueNumber': 1,
        'variantDescription': '',
        'description': null,
        'modified': '2021-10-29T11:58:36-0400',
        'isbn': '',
        'upc': '759606202011000111',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Comic',
        'pageCount': 40,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/95900',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/95900/death_of_doctor_strange_avengers_2021_1?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/Death-Of-Doctor-Strange-Avengers-1/digital-comic/57779?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/32871',
          'name': 'Death Of Doctor Strange: Avengers (2021)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2021-11-03T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2021-10-04T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 4.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/2/50/6169eeeb0aecd',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/2/50/6169eeeb0aecd',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/95900/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4574',
              'name': 'Ryan Bodenheim',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/14061',
              'name': 'Alex Paknadel',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12980',
              'name': 'Vc Cory Petit',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12991',
              'name': 'Rachelle Rosenberg',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13027',
              'name': 'Darren Shan',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/1249',
              'name': 'Steve Skroce',
              'role': 'penciler (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/501',
              'name': 'Dave Stewart',
              'role': 'colorist (cover)',
            },
          ],
          'returned': 7,
        },
        'characters': {
          'available': 4,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/95900/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009165',
              'name': 'Avengers',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009220',
              'name': 'Captain America',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009282',
              'name': 'Doctor Strange',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
          ],
          'returned': 4,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/95900/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/212864',
              'name': 'cover from new series (2021) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/212865',
              'name': 'story from new series (2021) #1',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/95900/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 93331,
        'digitalId': 0,
        'title': 'Shang-Chi (2021) #5',
        'issueNumber': 5,
        'variantDescription': '',
        'description': null,
        'modified': '2021-06-30T09:11:53-0400',
        'isbn': '',
        'upc': '75960620101300511',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/93331',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/93331/shang-chi_2021_5?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/Shang-Chi-5/digital-comic/57802?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/31902',
          'name': 'Shang-Chi (2021 - Present)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2021-10-13T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2021-09-20T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 3.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/20/6160b671cb3ff',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/9/20/6160b671cb3ff',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93331/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13518',
              'name': 'Triona Farrell',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/11579',
              'name': 'Sunny Gho',
              'role': 'colorist (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12993',
              'name': 'Vc Travis Lanham',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13932',
              'name': 'Dike Ruan',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13027',
              'name': 'Darren Shan',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/14041',
              'name': 'Gene Yang',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/758',
              'name': 'Leinil Francis Yu',
              'role': 'penciler (cover)',
            },
          ],
          'returned': 7,
        },
        'characters': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93331/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009577',
              'name': 'Shang-Chi',
            },
          ],
          'returned': 2,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93331/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/207774',
              'name': 'cover from Shang-Chi (2021) #5',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/207775',
              'name': 'story from Shang-Chi (2021) #5',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/93331/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 89679,
        'digitalId': 0,
        'title': 'Iron Man (2020) #13',
        'issueNumber': 13,
        'variantDescription': '',
        'description': null,
        'modified': '2021-06-30T09:09:36-0400',
        'isbn': '',
        'upc': '75960609866801311',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/89679',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/89679/iron_man_2020_13?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/Iron-Man-13/digital-comic/57788?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/30148',
          'name': 'Iron Man (2020 - Present)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2021-10-13T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2021-09-20T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 3.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/6/70/6160b68bc7f16',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/6/70/6160b68bc7f16',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 6,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/89679/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/2133',
              'name': 'Tom Brevoort',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13689',
              'name': 'C Cafu',
              'role': 'inker',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13861',
              'name': 'Christopher Cantwell',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/5251',
              'name': 'Vc Joe Caramagna',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/8504',
              'name': "Frank D'ARMATA",
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/63',
              'name': 'Alex Ross',
              'role': 'painter (cover)',
            },
          ],
          'returned': 6,
        },
        'characters': {
          'available': 5,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/89679/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010351',
              'name': 'Hellcat (Patsy Walker)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1011312',
              'name': 'Korvac',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010682',
              'name': 'Misty Knight',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1017834',
              'name': 'War Machine (James Rhodes)',
            },
          ],
          'returned': 5,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/89679/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/199675',
              'name': 'cover from Iron Man (2020) #13',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/199676',
              'name': 'story from Iron Man (2020) #13',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/89679/events',
          'items': [],
          'returned': 0,
        },
      },
      {
        'id': 84237,
        'digitalId': 0,
        'title': 'The Darkhold: Iron Man (2021) #1',
        'issueNumber': 1,
        'variantDescription': '',
        'description': null,
        'modified': '2021-10-11T12:43:21-0400',
        'isbn': '',
        'upc': '75960609831600111',
        'diamondCode': '',
        'ean': '',
        'issn': '',
        'format': 'Comic',
        'pageCount': 32,
        'textObjects': [],
        'resourceURI': 'http://gateway.marvel.com/v1/public/comics/84237',
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/issue/84237/the_darkhold_iron_man_2021_1?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'purchase',
            'url':
              'http://comicstore.marvel.com/The-Darkhold-Iron-Man-1/digital-comic/57815?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
        'series': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/series/29661',
          'name': 'The Darkhold: Iron Man (2021)',
        },
        'variants': [],
        'collections': [],
        'collectedIssues': [],
        'dates': [
          {
            'type': 'onsaleDate',
            'date': '2021-10-13T00:00:00-0400',
          },
          {
            'type': 'focDate',
            'date': '2021-09-20T00:00:00-0400',
          },
        ],
        'prices': [
          {
            'type': 'printPrice',
            'price': 3.99,
          },
        ],
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/70/6160b68f7acce',
          'extension': 'jpg',
        },
        'images': [
          {
            'path': 'http://i.annihil.us/u/prod/marvel/i/mg/c/70/6160b68f7acce',
            'extension': 'jpg',
          },
        ],
        'creators': {
          'available': 6,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/84237/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/10172',
              'name': 'Vc Clayton Cowles',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13250',
              'name': 'Romulo Fajardo Jr.',
              'role': 'colorist (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13936',
              'name': 'Valerio Giangiordano',
              'role': 'penciler (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12376',
              'name': 'Wilson Moss',
              'role': 'editor',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/12465',
              'name': 'Ryan North',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/13029',
              'name': 'Guillermo Sanna',
              'role': 'inker',
            },
          ],
          'returned': 6,
        },
        'characters': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/84237/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
          ],
          'returned': 1,
        },
        'stories': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/84237/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/186262',
              'name': 'cover from The Darkhold: TBD a (2020) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/186263',
              'name': 'story from The Darkhold: TBD a (2020) #1',
              'type': 'interiorStory',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/comics/84237/events',
          'items': [],
          'returned': 0,
        },
      },
    ],
  },
};

export const mockCharacterStories = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': 'b29cdd869e7bc5c08f859d74641f008e07898c34',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 3546,
    'count': 5,
    'results': [
      {
        'id': 670,
        'title': 'X-MEN (2004) #186',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/670',
        'type': 'cover',
        'modified': '2017-02-27T11:41:03-0500',
        'thumbnail': null,
        'creators': {
          'available': 6,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/670/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/393',
              'name': 'Casey Jones',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/11757',
              'name': 'Salvador Larroca',
              'role': 'penciller (cover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/94',
              'name': 'Peter Milligan',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/361',
              'name': 'Cory Petit',
              'role': 'letterer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/561',
              'name': 'Wil Quintana',
              'role': 'colorist',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/5973',
              'name': 'Vince Russel',
              'role': 'inker',
            },
          ],
          'returned': 6,
        },
        'characters': {
          'available': 8,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/670/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009175',
              'name': 'Beast',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009220',
              'name': 'Captain America',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009257',
              'name': 'Cyclops',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009337',
              'name': 'Havok',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009546',
              'name': 'Rogue',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009718',
              'name': 'Wolverine',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009726',
              'name': 'X-Men',
            },
          ],
          'returned': 8,
        },
        'series': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/670/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/403',
              'name': 'X-Men (2004 - 2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1644',
              'name': 'X-Men: Blood of Apocalypse (2006)',
            },
          ],
          'returned': 2,
        },
        'comics': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/670/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4181',
              'name': 'X-Men (2004) #186',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4441',
              'name': 'X-Men: Blood of Apocalypse (Trade Paperback)',
            },
          ],
          'returned': 2,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/670/events',
          'items': [],
          'returned': 0,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4181',
          'name': 'X-Men (2004) #186',
        },
      },
      {
        'id': 892,
        'title': 'THOR (1998) #81',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/892',
        'type': 'cover',
        'modified': '2017-09-27T10:35:28-0400',
        'thumbnail': null,
        'creators': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/892/creators',
          'items': [],
          'returned': 0,
        },
        'characters': {
          'available': 4,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/892/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009165',
              'name': 'Avengers',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009220',
              'name': 'Captain America',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009664',
              'name': 'Thor',
            },
          ],
          'returned': 4,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/892/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/581',
              'name': 'Thor (1998 - 2004)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/892/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/715',
              'name': 'Thor (1998) #81',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/892/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/234',
              'name': 'Avengers Disassembled',
            },
          ],
          'returned': 1,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/715',
          'name': 'Thor (1998) #81',
        },
      },
      {
        'id': 960,
        'title': '3 of ?',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/960',
        'type': 'cover',
        'modified': '1969-12-31T19:00:00-0500',
        'thumbnail': null,
        'creators': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/960/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/436',
              'name': 'Adi Granov',
              'role': 'penciller (cover)',
            },
          ],
          'returned': 1,
        },
        'characters': {
          'available': 4,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/960/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009220',
              'name': 'Captain America',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009366',
              'name': 'Invisible Woman',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009459',
              'name': 'Mr. Fantastic',
            },
          ],
          'returned': 4,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/960/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/421',
              'name': 'Fantastic Four (1998 - 2012)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/960/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4262',
              'name': 'Fantastic Four (1998) #538',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/960/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/238',
              'name': 'Civil War',
            },
          ],
          'returned': 1,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4262',
          'name': 'Fantastic Four (1998) #538',
        },
      },
      {
        'id': 982,
        'title': 'Interior #982',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/982',
        'type': 'story',
        'modified': '1969-12-31T19:00:00-0500',
        'thumbnail': null,
        'creators': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/982/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/4261',
              'name': 'Jorge Pereira Lucas',
              'role': 'penciller',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/8795',
              'name': 'John Miller',
              'role': 'writer',
            },
          ],
          'returned': 2,
        },
        'characters': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/982/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
          ],
          'returned': 1,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/982/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2572',
              'name': 'Iron Man (1998 - 2004)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/982/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/407',
              'name': 'Iron Man (1998) #78',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/982/events',
          'items': [],
          'returned': 0,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/407',
          'name': 'Iron Man (1998) #78',
        },
      },
      {
        'id': 984,
        'title': 'Interior #984',
        'description': '',
        'resourceURI': 'http://gateway.marvel.com/v1/public/stories/984',
        'type': 'story',
        'modified': '1969-12-31T19:00:00-0500',
        'thumbnail': null,
        'creators': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/984/creators',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/8795',
              'name': 'John Miller',
              'role': 'writer',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/creators/489',
              'name': 'Philip Tan',
              'role': 'penciller',
            },
          ],
          'returned': 2,
        },
        'characters': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/984/characters',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009368',
              'name': 'Iron Man',
            },
          ],
          'returned': 1,
        },
        'series': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/984/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2572',
              'name': 'Iron Man (1998 - 2004)',
            },
          ],
          'returned': 1,
        },
        'comics': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/984/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/470',
              'name': 'Iron Man (1998) #79',
            },
          ],
          'returned': 1,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/stories/984/events',
          'items': [],
          'returned': 0,
        },
        'originalIssue': {
          'resourceURI': 'http://gateway.marvel.com/v1/public/comics/470',
          'name': 'Iron Man (1998) #79',
        },
      },
    ],
  },
};
