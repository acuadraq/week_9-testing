/* eslint-disable quote-props */
const characterStoryFilter = {
  'code': 200,
  'status': 'Ok',
  'copyright': '© 2021 MARVEL',
  'attributionText': 'Data provided by Marvel. © 2021 MARVEL',
  'attributionHTML': '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  'etag': '9b758e34c55d0f6a0260d41fcec6adc487c70310',
  'data': {
    'offset': 0,
    'limit': 5,
    'total': 9,
    'count': 5,
    'results': [
      {
        'id': 1017100,
        'name': 'A-Bomb (HAS)',
        'description':
          "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
        'modified': '2013-09-18T15:54:04-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1017100',
        'comics': {
          'available': 4,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1017100/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/47176',
              'name': 'FREE COMIC BOOK DAY 2013 1 (2013) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40632',
              'name': 'Hulk (2008) #53',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40630',
              'name': 'Hulk (2008) #54',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40628',
              'name': 'Hulk (2008) #55',
            },
          ],
          'returned': 4,
        },
        'series': {
          'available': 2,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1017100/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/17765',
              'name': 'FREE COMIC BOOK DAY 2013 1 (2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3374',
              'name': 'Hulk (2008 - 2012)',
            },
          ],
          'returned': 2,
        },
        'stories': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1017100/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92078',
              'name': 'Hulk (2008) #55',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92079',
              'name': 'Interior #92079',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92082',
              'name': 'Hulk (2008) #54',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92083',
              'name': 'Interior #92083',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92086',
              'name': 'Hulk (2008) #53',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92087',
              'name': 'Interior #92087',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/105929',
              'name': 'cover from Free Comic Book Day 2013 (Avengers/Hulk) (2013) #1',
              'type': 'cover',
            },
          ],
          'returned': 7,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1017100/events',
          'items': [],
          'returned': 0,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/characters/76/a-bomb?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1017100/a-bomb_has?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1010370,
        'name': 'Alpha Flight',
        'description': '',
        'modified': '2013-10-24T13:09:22-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/1/60/52695277ee088',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010370',
        'comics': {
          'available': 208,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010370/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/39654',
              'name': 'Alpha Flight (2011) #0.1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/393',
              'name': 'Alpha Flight (2004) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12637',
              'name': 'Alpha Flight (1983) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/39819',
              'name': 'Alpha Flight (2011) #1 (Eaglesham Variant)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12679',
              'name': 'Alpha Flight (1983) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/456',
              'name': 'Alpha Flight (2004) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38569',
              'name': 'Alpha Flight (2011) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/39818',
              'name': 'Alpha Flight (2011) #2 (Eaglesham Variant)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12690',
              'name': 'Alpha Flight (1983) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/616',
              'name': 'Alpha Flight (2004) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/39820',
              'name': 'Alpha Flight (2011) #3 (Eaglesham Variant)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/677',
              'name': 'Alpha Flight (2004) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12701',
              'name': 'Alpha Flight (1983) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38567',
              'name': 'Alpha Flight (2011) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38568',
              'name': 'Alpha Flight (2011) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12712',
              'name': 'Alpha Flight (1983) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/613',
              'name': 'Alpha Flight (2004) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/38566',
              'name': 'Alpha Flight (2011) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/572',
              'name': 'Alpha Flight (2004) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/12723',
              'name': 'Alpha Flight (1983) #6',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 40,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010370/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13907',
              'name': 'Alpha Flight (2011 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2116',
              'name': 'Alpha Flight (1983 - 1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/719',
              'name': 'Alpha Flight (2004 - 2005)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27041',
              'name': 'Alpha Flight Facsimile Edition (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27625',
              'name': 'Alpha Flight: True North (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/28069',
              'name': 'Annihilation: Scourge (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/744',
              'name': 'Astonishing X-Men (2004 - 2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1991',
              'name': 'Avengers (1963 - 1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1995',
              'name': 'Cable (1993 - 2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20718',
              'name': 'Captain Marvel (2016 - 2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22552',
              'name': 'Champions (2016 - 2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/11854',
              'name': 'Chaos War (2010 - 2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13468',
              'name': 'Chaos War One-Shots (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13260',
              'name': 'Chaos War: Alpha Flight (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/21692',
              'name': 'Civil War II: Choosing Sides (2016)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/14399',
              'name': 'Essential X-Men Vol. 2 (All-New Edition) (2011 - Present)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2123',
              'name': 'Fantastic Four (1996 - 1997)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2121',
              'name': 'Fantastic Four (1961 - 1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20465',
              'name': 'Guardians of the Galaxy (2015 - 2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1812',
              'name': 'Heroes Reborn: Fantastic Four (2006)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 378,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010370/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2921',
              'name': 'Alpha Flight (2004) #9',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2922',
              'name': '1 of 4 - Days of Future Present Past Participle',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2923',
              'name': 'Alpha Flight (2004) #1',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2924',
              'name': 'Interior #2924',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2925',
              'name': 'Alpha Flight (2004) #2',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2926',
              'name': 'Interior #2926',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2927',
              'name': 'Alpha Flight (2004) #6',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2928',
              'name': 'Interior #2928',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2929',
              'name': 'Alpha Flight (2004) #5',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2930',
              'name': 'Interior #2930',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2931',
              'name': 'Alpha Flight (2004) #3',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2932',
              'name': 'Interior #2932',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2933',
              'name': 'Alpha Flight (2004) #4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2934',
              'name': 'Interior #2934',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2935',
              'name': 'Alpha Flight (2004) #7',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2936',
              'name':
                '"WAXING POETIC" PART 1 (OF 2) Is the All-New, All-Different Alpha Flight really disbanding after only seven issues? Not if the r',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2937',
              'name': 'Alpha Flight (2004) #8',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2938',
              'name':
                '"WAXING POETIC" PART 2 (OF 2) Montreal faces its gravest hour as it falls under attack by…wax statues of the entire Marvel Unive',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2939',
              'name': 'Alpha Flight (2004) #10',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/2940',
              'name': '2 of 4 - Days of Future Present Past Participle',
              'type': 'interiorStory',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 7,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010370/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/116',
              'name': 'Acts of Vengeance!',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/296',
              'name': 'Chaos War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/302',
              'name': 'Fear Itself',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/29',
              'name': 'Infinity War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/337',
              'name': 'Monsters Unleashed',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/333',
              'name': 'Monsters Unleashed',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/271',
              'name': 'Secret Wars II',
            },
          ],
          'returned': 7,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1010370/alpha_flight?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Alpha_Flight?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1010370/alpha_flight?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1009220,
        'name': 'Captain America',
        'description':
          "Vowing to serve his country any way he could, young Steve Rogers took the super soldier serum to become America's one-man army. Fighting for the red, white and blue for over 60 years, Captain America is the living, breathing symbol of freedom and liberty.",
        'modified': '2020-04-04T19:01:59-0400',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/537ba56d31087',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1009220',
        'comics': {
          'available': 2393,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009220/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/43488',
              'name': 'A+X (2012) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/43501',
              'name': 'A+X (2012) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/43508',
              'name': 'A+X (2012) #9',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17743',
              'name': 'A-Next (1998) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17744',
              'name': 'A-Next (1998) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17745',
              'name': 'A-Next (1998) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17746',
              'name': 'A-Next (1998) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17747',
              'name': 'A-Next (1998) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17748',
              'name': 'A-Next (1998) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17749',
              'name': 'A-Next (1998) #8',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17750',
              'name': 'A-Next (1998) #9',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17740',
              'name': 'A-Next (1998) #10',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17741',
              'name': 'A-Next (1998) #11',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17742',
              'name': 'A-Next (1998) #12',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66978',
              'name': 'Adventures of Captain America (1991) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66979',
              'name': 'Adventures of Captain America (1991) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66980',
              'name': 'Adventures of Captain America (1991) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/66981',
              'name': 'Adventures of Captain America (1991) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/42539',
              'name': 'Age of Apocalypse (2011) #2 (Avengers Art Appreciation Variant)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/30090',
              'name': 'Age of Heroes (2010) #1',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 673,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009220/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/16450',
              'name': 'A+X (2012 - 2014)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3620',
              'name': 'A-Next (1998 - 1999)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24227',
              'name': 'Adventures of Captain America (1991 - 1992)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/15331',
              'name': 'Age of Apocalypse (2011 - 2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9790',
              'name': 'Age of Heroes (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/10235',
              'name': 'AGE OF HEROES TPB (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/13896',
              'name': 'Age of X: Universe (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/7534',
              'name': 'All Winners Comics 70th Anniversary Special (2009)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/20682',
              'name': 'All-New Wolverine (2015 - 2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2114',
              'name': 'All-Winners Comics (1941 - 1947)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9865',
              'name': 'All-Winners Squad: Band of Heroes (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2116',
              'name': 'Alpha Flight (1983 - 1994)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/25984',
              'name': 'Amazing Fantasy (2021 - Present)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/454',
              'name': 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9802',
              'name': 'Amazing Spider-Man Annual (2010)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2984',
              'name': 'Amazing Spider-Man Annual (1964 - 2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1489',
              'name': 'AMAZING SPIDER-MAN VOL. 10: NEW AVENGERS TPB (2005)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/14818',
              'name': 'Annihilators: Earthfall (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24323',
              'name': 'Ant-Man and the Wasp Adventures (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/14696',
              'name':
                'Art of Marvel Movies: The Art of Captain America - The First Avenger (2011 - Present)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 3558,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009220/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/670',
              'name': 'X-MEN (2004) #186',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/892',
              'name': 'THOR (1998) #81',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/960',
              'name': '3 of ?',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1026',
              'name': 'Avengers (1998) #81',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1041',
              'name': 'Avengers (1998) #502',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1042',
              'name': 'Avengers (1998) #503',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1054',
              'name': 'Avengers (1998) #72',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1163',
              'name': 'Amazing Spider-Man (1999) #519',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1165',
              'name': 'Amazing Spider-Man (1999) #520',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1167',
              'name': 'Amazing Spider-Man (1999) #521',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1175',
              'name': 'Amazing Spider-Man (1999) #523',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1193',
              'name': 'Amazing Spider-Man (1999) #534',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1199',
              'name': 'Amazing Spider-Man (1999) #537',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1203',
              'name': 'Amazing Spider-Man (1999) #538',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1414',
              'name': 'Cover #1414',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1500',
              'name': 'Interior #1500',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1501',
              'name': 'CAPTAIN AMERICA (2002) #21',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1503',
              'name': 'CAPTAIN AMERICA (2002) #22',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1505',
              'name': 'CAPTAIN AMERICA (2002) #23',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/1606',
              'name': 'WEAPON X (2002) #14',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 31,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1009220/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/116',
              'name': 'Acts of Vengeance!',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/314',
              'name': 'Age of Ultron',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/303',
              'name': 'Age of X',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/231',
              'name': 'Armor Wars',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/234',
              'name': 'Avengers Disassembled',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/310',
              'name': 'Avengers VS X-Men',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/320',
              'name': 'Axis',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/296',
              'name': 'Chaos War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/238',
              'name': 'Civil War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/248',
              'name': 'Fall of the Mutants',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/302',
              'name': 'Fear Itself',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/251',
              'name': 'House of M',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/252',
              'name': 'Inferno',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/315',
              'name': 'Infinity',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/29',
              'name': 'Infinity War',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/317',
              'name': 'Inhumanity',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/151',
              'name': 'Maximum Carnage',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/37',
              'name': 'Maximum Security',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/337',
              'name': 'Monsters Unleashed',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/333',
              'name': 'Monsters Unleashed',
            },
          ],
          'returned': 20,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1009220/captain_america?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Captain_America_(Steve_Rogers)?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1009220/captain_america?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1010854,
        'name': 'Living Lightning',
        'description': '',
        'modified': '1969-12-31T19:00:00-0500',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/4c0035c72cc26',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010854',
        'comics': {
          'available': 21,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010854/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17490',
              'name': 'Avengers (1998) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17501',
              'name': 'Avengers (1998) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17512',
              'name': 'Avengers (1998) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17523',
              'name': 'Avengers (1998) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17509',
              'name': 'Avengers (1998) #27',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17521',
              'name': 'Avengers (1998) #38',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17531',
              'name': 'Avengers (1998) #47',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17532',
              'name': 'Avengers (1998) #48',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17535',
              'name': 'Avengers (1998) #50',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17537',
              'name': 'Avengers (1998) #52',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17538',
              'name': 'Avengers (1998) #53',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17540',
              'name': 'Avengers (1998) #55',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/1311',
              'name': 'Avengers Assemble (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/4461',
              'name': 'Avengers Assemble Vol. 3 (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/5844',
              'name': 'Avengers Assemble Vol. 4 (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17844',
              'name': 'West Coast Avengers Annual (1986) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17845',
              'name': 'West Coast Avengers Annual (1986) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/5368',
              'name': 'Avengers: Galactic Storm Vol. 2 (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/1038',
              'name': 'AVENGERS: THE KANG DYNASTY TPB (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/40628',
              'name': 'Hulk (2008) #55',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 9,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010854/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/354',
              'name': 'Avengers (1998 - 2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1340',
              'name': 'Avengers Assemble (2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1737',
              'name': 'Avengers Assemble Vol. 3 (2006)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1816',
              'name': 'Avengers Assemble Vol. 4 (2007)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1679',
              'name': 'Avengers: Galactic Storm Vol. 2 (2006)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/98',
              'name': 'AVENGERS: THE KANG DYNASTY TPB (2002)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3374',
              'name': 'Hulk (2008 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2083',
              'name': 'Thor (1966 - 1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3632',
              'name': 'West Coast Avengers Annual (1986 - 1988)',
            },
          ],
          'returned': 9,
        },
        'stories': {
          'available': 21,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010854/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/17229',
              'name': 'Now Strikes the Starforce',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37337',
              'name': 'Cover #37337',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37338',
              'name': 'Once an Avenger...',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37359',
              'name': 'Avengers (1998) #2',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37360',
              'name': 'The Call',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37376',
              'name': 'New Order',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37386',
              'name': 'Fata Morgana',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37405',
              'name': 'Above and Beyond',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37408',
              'name': 'Avengers (1998) #4',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37409',
              'name': 'Too Many Avengers!',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37426',
              'name': 'In the Heart of Battle',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37428',
              'name': 'War Plan "A"',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37438',
              'name': 'Book of Revelations',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37442',
              'name': 'Counter Attack',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37444',
              'name': 'The Last Castle',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37449',
              'name': 'The Last Farewell',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37920',
              'name': 'Justice, Like Lightning',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37926',
              'name': 'My Name Is Legion',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/67077',
              'name': 'Thor (1966) #446',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/92078',
              'name': 'Hulk (2008) #55',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 0,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010854/events',
          'items': [],
          'returned': 0,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/characters/2841/living_lightning?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Living%20Lightning?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1010854/living_lightning?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
      {
        'id': 1010805,
        'name': 'Machine Man',
        'description': '',
        'modified': '2012-01-18T12:42:25-0500',
        'thumbnail': {
          'path': 'http://i.annihil.us/u/prod/marvel/i/mg/f/d0/4c003727804b4',
          'extension': 'jpg',
        },
        'resourceURI': 'http://gateway.marvel.com/v1/public/characters/1010805',
        'comics': {
          'available': 94,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010805/comics',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/84341',
              'name': '2020 Iron Age (2020) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/84349',
              'name': '2020 Machine Man (2020) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17490',
              'name': 'Avengers (1998) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17501',
              'name': 'Avengers (1998) #2',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17512',
              'name': 'Avengers (1998) #3',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/17523',
              'name': 'Avengers (1998) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/1311',
              'name': 'Avengers Assemble (Hardcover)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65952',
              'name': 'Cable/Machine Man Annual (1998) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/65120',
              'name': 'Cable: The Hellfire Hunt (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/67950',
              'name':
                'Captain Marvel: Carol Danvers - The Ms. Marvel Years Vol. 2 (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/59966',
              'name': 'Deadpool & the Mercs for Money (2016) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/59967',
              'name': 'Deadpool & the Mercs for Money (2016) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/59968',
              'name': 'Deadpool & the Mercs for Money (2016) #6',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/62508',
              'name': 'Deadpool & the Mercs for Money (2016) #7',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/62586',
              'name': 'Deadpool & the Mercs for Money (2016) #8',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/30856',
              'name': 'Deadpool Team-Up Vol. 2: Special Relationship (Trade Paperback)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8548',
              'name': 'Earth X (1999) #1',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8554',
              'name': 'Earth X (1999) #4',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8555',
              'name': 'Earth X (1999) #5',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/comics/8556',
              'name': 'Earth X (1999) #6',
            },
          ],
          'returned': 20,
        },
        'series': {
          'available': 27,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010805/series',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/29692',
              'name': '2020 Iron Age (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/29695',
              'name': '2020 Machine Man (2020)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/354',
              'name': 'Avengers (1998 - 2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1340',
              'name': 'Avengers Assemble (2004)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23926',
              'name': 'Cable/Machine Man Annual (1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23685',
              'name': 'Cable: The Hellfire Hunt (2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/24528',
              'name': 'Captain Marvel: Carol Danvers - The Ms. Marvel Years Vol. 2 (2018)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/21942',
              'name': 'Deadpool & the Mercs for Money (2016 - 2017)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/10144',
              'name': 'Deadpool Team-Up Vol. 2: Special Relationship (2011)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/378',
              'name': 'Earth X (1999 - 2000)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/1806',
              'name': 'EARTH X TPB [NEW PRINTING] (2006)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/3374',
              'name': 'Hulk (2008 - 2012)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2029',
              'name': 'Iron Man (1968 - 1996)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/27110',
              'name': 'Kirby Returns! King-Size (2019)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/26525',
              'name': 'Machine Man (1984 - 1985)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/22669',
              'name': 'Machine Man (1978 - 1981)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/23927',
              'name': 'Machine Man/Bastion Annual (1998)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2039',
              'name': 'Marvel Comics Presents (1988 - 1995)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/2815',
              'name': 'Marvel Comics Presents (2007 - 2008)',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/series/9719',
              'name': 'Marvel Zombies 5 (2010)',
            },
          ],
          'returned': 20,
        },
        'stories': {
          'available': 115,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010805/stories',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19284',
              'name': 'Iron Man (1968) #168',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/19285',
              'name': 'The Iron Scream',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/22676',
              'name': 'Machine Man Meets the F. F. ... Failure Five',
              'type': '',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24902',
              'name': 'Cover #24902',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24903',
              'name': 'Earth X',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24904',
              'name': 'Cover #24904',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24907',
              'name': 'Cover #24907',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24924',
              'name': 'Appendix to Chapter Four',
              'type': 'text story',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24927',
              'name': 'Appendix to Chapter Five',
              'type': 'text story',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24930',
              'name': 'Appendix to Chapter Six',
              'type': 'text story',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24933',
              'name': 'Appendix to Chapter Seven',
              'type': 'text story',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/24936',
              'name': 'Appendix to Chapter Eight',
              'type': 'text story',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/26017',
              'name': 'Cover #26017',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/26039',
              'name': 'Cover #26039',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37337',
              'name': 'Cover #37337',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37338',
              'name': 'Once an Avenger...',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37359',
              'name': 'Avengers (1998) #2',
              'type': 'cover',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37360',
              'name': 'The Call',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37386',
              'name': 'Fata Morgana',
              'type': 'interiorStory',
            },
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/stories/37408',
              'name': 'Avengers (1998) #4',
              'type': 'cover',
            },
          ],
          'returned': 20,
        },
        'events': {
          'available': 1,
          'collectionURI': 'http://gateway.marvel.com/v1/public/characters/1010805/events',
          'items': [
            {
              'resourceURI': 'http://gateway.marvel.com/v1/public/events/334',
              'name': 'Inhumans Vs. X-Men',
            },
          ],
          'returned': 1,
        },
        'urls': [
          {
            'type': 'detail',
            'url':
              'http://marvel.com/comics/characters/1010805/machine_man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'wiki',
            'url':
              'http://marvel.com/universe/Machine_Man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
          {
            'type': 'comiclink',
            'url':
              'http://marvel.com/comics/characters/1010805/machine_man?utm_campaign=apiRef&utm_source=2a71e98d4c9696c23c76c0d5cff81bb8',
          },
        ],
      },
    ],
  },
};

export default characterStoryFilter;
