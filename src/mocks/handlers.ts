// eslint-disable-next-line import/no-extraneous-dependencies
import { rest } from 'msw';
import {
  mockCharacterDetail,
  mockCharacterComics,
  mockCharacterStories,
} from './jsons/characterDetail';
import mockFilterCharacter from './jsons/characterFilter/characterFilter';
import characterComicFilter from './jsons/characterFilter/characterComicFiler';
import characterStoryFilter from './jsons/characterFilter/characterStoryFilter';
import mockCharacters from './jsons/characters';
import mockComics from './jsons/comics';
import mockStories from './jsons/stories';
import comicNameFilter from './jsons/comicFilter/comicFilter';
import comicFormatFilter from './jsons/comicFilter/comicFormat';
import { mockStoryDetail, mockStoryCharacters, mockStoryComics } from './jsons/storyDetail';
import { mockComicDetail, mockComicCharacters, mockComicStories } from './jsons/ComicDetail';

const handlers = [
  rest.get('https://gateway.marvel.com/v1/public/characters', (req, res, ctx) => {
    const nameParam = req.url.searchParams.get('nameStartsWith');
    if (nameParam) return res(ctx.status(200), ctx.json(mockFilterCharacter));
    const comicParam = req.url.searchParams.get('comics');
    if (comicParam) return res(ctx.status(200), ctx.json(characterComicFilter));
    const storiesParam = req.url.searchParams.get('stories');
    if (storiesParam) return res(ctx.status(200), ctx.json(characterStoryFilter));
    return res(ctx.status(200), ctx.json(mockCharacters));
  }),

  rest.get('https://gateway.marvel.com/v1/public/comics', (req, res, ctx) => {
    const titleParam = req.url.searchParams.get('titleStartsWith');
    if (titleParam) return res(ctx.status(200), ctx.json(comicNameFilter));
    const formatParam = req.url.searchParams.get('format');
    if (formatParam) return res(ctx.status(200), ctx.json(comicFormatFilter));
    return res(ctx.status(200), ctx.json(mockComics));
  }),

  rest.get('https://gateway.marvel.com/v1/public/stories', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockStories));
  }),

  rest.get('https://gateway.marvel.com/v1/public/characters/1009368', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockCharacterDetail));
  }),

  rest.get('https://gateway.marvel.com/v1/public/characters/1009368/comics', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockCharacterComics));
  }),

  rest.get('https://gateway.marvel.com/v1/public/characters/1009368/stories', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockCharacterStories));
  }),

  rest.get('https://gateway.marvel.com/v1/public/comics/93571', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockComicDetail));
  }),

  rest.get('https://gateway.marvel.com/v1/public/comics/93571/characters', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockComicCharacters));
  }),

  rest.get('https://gateway.marvel.com/v1/public/comics/93571/stories', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockComicStories));
  }),

  rest.get('https://gateway.marvel.com/v1/public/stories/25637', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockStoryDetail));
  }),

  rest.get('https://gateway.marvel.com/v1/public/stories/25637/characters', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockStoryCharacters));
  }),

  rest.get('https://gateway.marvel.com/v1/public/stories/25637/comics', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockStoryComics));
  }),
];

export default handlers;
