import React, { useState } from 'react';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';

type filterFunc = {
  getFunction: (queries?: string) => void;
};

const Filter = ({ getFunction }: filterFunc) => {
  const urlSearch = new URL(window.location.href);
  const [query, setQuery] = useState<string>('');
  const navigate = useNavigate();
  const params = urlSearch.search;

  const setTitle = (value: string) => {
    urlSearch.searchParams.set('titleStartsWith', value);
    navigate(`${urlSearch.search}`);
  };

  const setFormat = (value: string) => {
    urlSearch.searchParams.set('format', value);
    navigate(`${urlSearch.search}`);
  };

  const getTitle = debounce(setTitle, 900);

  if (params) {
    const titleParam = urlSearch.searchParams.get('titleStartsWith');
    const formatParam = urlSearch.searchParams.get('format');
    const queryNew = `${titleParam ? `&titleStartsWith=${titleParam}` : ''}${
      formatParam ? `&format=${formatParam}` : ''
    }`;
    if (query !== queryNew) {
      getFunction(queryNew);
      setQuery(queryNew);
    }
  }

  return (
    <div className="filter__container">
      <p>Filter By:</p>
      <hr />
      <div>
        <label htmlFor="title">
          Title
          <input
            type="text"
            name="title"
            id="title"
            placeholder="Comics title"
            onChange={e => getTitle(e.target.value)}
          />
        </label>
      </div>
      <div>
        <label htmlFor="format">
          Format
          <select
            name="format"
            aria-label="format-select"
            id="format"
            onChange={e => setFormat(e.target.value)}
          >
            <option value="">All</option>
            <option value="comic">Comic</option>
            <option value="magazine">Magazine</option>
            <option value="trade paperback">Trade Paperback</option>
            <option value="hardcover">Hardcover</option>
            <option value="digest">Digest</option>
            <option value="graphic novel">Graphic Novel</option>
            <option value="digital comic">Digital Comic</option>
            <option value="infinite comic">Infinite Comic</option>
          </select>
        </label>
      </div>
    </div>
  );
};

export default Filter;
