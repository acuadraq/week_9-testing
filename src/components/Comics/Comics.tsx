/* eslint-disable operator-linebreak */
import React, { useState, useEffect } from 'react';
import { getComics } from '../../adapters/Fetch';
import { IComic } from '../../interfaces/comics';
import Comic from './Comic';
import Filter from './Filter';

const Comics = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [comics, setComics] = useState<IComic>();
  const [error, setError] = useState<string>('');

  const getComicInfo = (queries?: string) => {
    getComics(queries)
      .then(resp => {
        setComics(resp);
        setLoading(false);
      })
      .catch(err => {
        setError('There was an error try again');
        setLoading(false);
      });
  };

  useEffect(() => {
    setLoading(true);
    getComicInfo();
  }, []);

  return (
    <section>
      <div className="container">
        <div className="grid__container">
          <Filter getFunction={getComicInfo} />
          {loading && <div className="animation__loading" aria-label="Loading" />}
          {error && <p>{error}</p>}
          <Comic comicArray={comics} />
        </div>
      </div>
    </section>
  );
};
export default Comics;
