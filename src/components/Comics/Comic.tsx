/* eslint-disable operator-linebreak */
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { IComic, IComicResults } from '../../interfaces/comics';
import { AppState } from '../../redux/reducers';
import Pagination from '../Pagination';

type comicProps = {
  comicArray: IComic | undefined;
};

const Comic = ({ comicArray }: comicProps) => {
  const comics: IComicResults[] | undefined = comicArray?.data.results;
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const comicsPerPage: number = 9;

  const paginate = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  const indexOfLastComic: number = currentPage * comicsPerPage;
  const indexOfFirstComic: number = indexOfLastComic - comicsPerPage;
  const currentComics: IComicResults[] | undefined = comics?.slice(
    indexOfFirstComic,
    indexOfLastComic,
  );

  useEffect(() => {
    setCurrentPage(1);
  }, [comics]);

  return (
    <div>
      <div className="items__container">
        {currentComics?.length === 0 && <p>No Comics found</p>}
        {currentComics &&
          currentComics.map(comic => (
            <div
              key={comic.id}
              className={`item__div ${resourceHidden.includes(comic.id) ? 'display__none' : ''}`}
            >
              <div className="image__container">
                <Link to={`${comic.id}`}>
                  <img src={`${comic.thumbnail?.path}.${comic.thumbnail?.extension}`} alt="" />
                </Link>
              </div>
              <div className="text__container">
                <h5 aria-label={`${comic.title}`}>
                  <Link to={`${comic.id}`}>
                    {comic.title?.slice(0, 30)}
                    ...
                  </Link>
                </h5>
              </div>
            </div>
          ))}
      </div>

      <Pagination
        itemsPerPage={comicsPerPage}
        totalItems={comics?.length || 0}
        paginate={paginate}
        currentPage={currentPage}
      />
    </div>
  );
};
export default Comic;
