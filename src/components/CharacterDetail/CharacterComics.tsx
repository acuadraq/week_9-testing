import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { IComic } from '../../interfaces/comics';
import { AppState } from '../../redux/reducers';

type ComicsProps = {
  comics: IComic | undefined;
};

const CharacterComics = ({ comics }: ComicsProps) => {
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);

  return (
    <div className="char-comics__container">
      {comics?.data.results.map(comic => (
        <div
          key={comic.id}
          className={`item__div ${resourceHidden.includes(comic.id) ? 'display__none' : ''}`}
          aria-label={`${comic.title}`}
        >
          <div className="image__container">
            <Link to={`/comics/${comic.id}`} replace>
              <img
                src={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
                alt={`${comic.title}`}
              />
            </Link>
          </div>
          <div className="text__container">
            <h5>
              <Link to={`/comics/${comic.id}`}>
                {comic.title.slice(0, 30)}
                ...
              </Link>
            </h5>
          </div>
        </div>
      ))}
    </div>
  );
};
export default CharacterComics;
