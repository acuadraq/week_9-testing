/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable operator-linebreak */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { getCharacter, getCharacterComics, getCharacterStories } from '../../adapters/Fetch';
import { ICharacter } from '../../interfaces/characters';
import { IComic } from '../../interfaces/comics';
import { IStories } from '../../interfaces/stories';
import { removeBookmark, setBookmark, setHide } from '../../redux/actions/actions';
import NotFound from '../NotFound';
import CharacterComics from './CharacterComics';
import bookmark from '../bookmark.png';
import savedbookmark from '../saved-bookmark.png';
import CharacterStories from './CharacterStories';
import hide from '../hide.png';
import { AppState } from '../../redux/reducers';
import useRefresh from '../../customhooks/useRefresh';
import filterId from '../../helpers/filterId';

type Bookmark = {
  bookmark: {
    id: number | undefined;
    name: string | undefined;
    thumbnail: string | undefined;
    type: string | undefined;
  };
};

const CharacterDetail = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [characterInfo, setCharacterInfo] = useState<ICharacter>();
  const [error, setError] = useState<boolean>(false);
  const [comics, setComics] = useState<IComic>();
  const [stories, setStories] = useState<IStories>();
  const dispatch = useDispatch();
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);
  const bookmarks: Bookmark[] = useSelector((state: AppState) => state.bookmarks);
  const navigate = useNavigate();
  const { id } = useParams<string>();
  const refresh = useRefresh(navigate, `/characters/${id}`);

  const getStories = async () => {
    if (id) {
      const story: IStories = await getCharacterStories(parseInt(id, 10));
      await setStories(story);
      setLoading(false);
    }
  };

  const getComics = async () => {
    if (id) {
      const comic: IComic = await getCharacterComics(parseInt(id, 10));
      await setComics(comic);
      getStories();
    }
  };

  const getCharacterInfo = async () => {
    if (id) {
      if (resourceHidden.includes(parseInt(id, 10))) {
        setError(true);
        setLoading(false);
        return;
      }
      const character: ICharacter = await getCharacter(parseInt(id, 10));
      if (character.code === 404) {
        setError(true);
        setLoading(false);
        return;
      }
      await setCharacterInfo(character);
      getComics();
    }
  };

  const saveBookmark = () => {
    const charac = {
      bookmark: {
        id: characterInfo?.data.results[0].id,
        name: characterInfo?.data.results[0].name,
        thumbnail: `${characterInfo?.data.results[0].thumbnail.path}.${characterInfo?.data.results[0].thumbnail.extension}`,
        type: 'characters',
      },
    };
    dispatch(setBookmark(charac));
  };

  const hideElement = (num: number) => {
    const isInBookmark = filterId(bookmarks, num);
    dispatch(setHide(num));
    if (isInBookmark !== -1) dispatch(removeBookmark(isInBookmark));
    refresh();
  };

  const bookmarkButton = (num: number) => {
    const isInBookmark = filterId(bookmarks, num);
    if (isInBookmark !== -1) return true;
    return false;
  };

  useEffect(() => {
    setLoading(true);
    getCharacterInfo();
  }, []);

  return (
    <section>
      <div className="container">
        {loading && <div className="animation__loading" aria-label="Loading" />}
        {!loading && (
          <>
            {error ? (
              <NotFound />
            ) : (
              characterInfo?.data.results.map(character => (
                <div key={character.id}>
                  <div className="character__banner">
                    <div className="image__container">
                      <img
                        src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                        alt={character.name}
                      />
                    </div>
                    <div className="character__content">
                      <h1>{character.name}</h1>
                      <div className="overview-button">
                        <span className="overview">Overview</span>
                        <button
                          aria-label="hide-item"
                          type="button"
                          onClick={() => hideElement(character.id)}
                        >
                          <span>Hide</span>
                          <img src={hide} alt="Hide" width="20" height="20" />
                        </button>
                      </div>
                      <p>
                        {character.description
                          ? character.description
                          : 'This character doesnt have a description'}
                      </p>
                      <div className="buttons">
                        <a href={character.urls[0].url} target="_blank">
                          Learn More
                        </a>
                        {bookmarkButton(character.id) ? (
                          <button type="button" onClick={saveBookmark} aria-label="item-saved">
                            <span>Saved</span>
                            <img src={savedbookmark} alt="Bookmark" width="20" height="20" />
                          </button>
                        ) : (
                          <button type="button" onClick={saveBookmark} aria-label="save-item">
                            <span>Save</span>
                            <img src={bookmark} alt="Bookmark" width="20" height="20" />
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="items__additionals">
                    <h2>
                      Comics of
                      {` ${character.name}`}
                    </h2>
                    <hr />
                    {comics?.data.results.length === 0 ? (
                      <p>This character doesnt have comics</p>
                    ) : (
                      <CharacterComics comics={comics} />
                    )}
                  </div>
                  <div className="items__additionals">
                    <h2>
                      Stories of
                      {` ${character.name}`}
                    </h2>
                    <hr />
                    {stories?.data.results.length === 0 ? (
                      <p>This character doesnt have stories</p>
                    ) : (
                      <CharacterStories stories={stories} />
                    )}
                  </div>
                </div>
              ))
            )}
          </>
        )}
      </div>
    </section>
  );
};
export default CharacterDetail;
