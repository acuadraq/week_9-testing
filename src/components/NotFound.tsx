import React from 'react';
import { Link } from 'react-router-dom';
import RoutesNames from '../customroutes/routesNames';
import notfound from './notfound.svg';
import back from './back.png';

const NotFound = () => {
  return (
    <div className="notfound__page">
      <h2>Page Not Found</h2>
      <img src={notfound} alt="" className="img__vector" />
      <Link to={RoutesNames.home}>
        <span>
          <img src={back} alt="" width="32" height="32" />
        </span>
        Go back home
      </Link>
    </div>
  );
};

export default NotFound;
