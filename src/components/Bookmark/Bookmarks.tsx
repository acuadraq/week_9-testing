import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { removeBookmark, removeAllBookmarks } from '../../redux/actions/actions';
import { AppState } from '../../redux/reducers';

type Bookmark = {
  bookmark: {
    id: number | undefined;
    name: string | undefined;
    thumbnail: string | undefined;
    type: string | undefined;
  };
};

const Bookmarks = () => {
  const dispatch = useDispatch();
  const bookmarkss: Bookmark[] = useSelector((state: AppState) => state.bookmarks);

  const removeAll = () => {
    if (bookmarkss.length === 0) return;
    dispatch(removeAllBookmarks());
  };

  return (
    <section>
      <div className="container">
        <div className="bookmarks__header">
          <h1 className="text-center">Bookmarks</h1>
          <button
            type="button"
            className={bookmarkss.length === 0 ? 'button--disabled' : 'button--active'}
            onClick={() => removeAll()}
          >
            Remove All
          </button>
        </div>
        <hr />
        <div className="bookmarks__container">
          {bookmarkss.length === 0 && <p>There is no bookmarks yet</p>}
          {bookmarkss.map((bookmark, index) => (
            <div key={bookmark.bookmark.id} className="bookmark__item">
              <div className="image__container">
                <Link to={`/${bookmark.bookmark.type}/${bookmark.bookmark.id}`}>
                  <img src={`${bookmark.bookmark.thumbnail}`} alt={bookmark.bookmark.name} />
                </Link>
              </div>
              <div className="text__container">
                <h5>
                  <Link to={`/${bookmark.bookmark.type}/${bookmark.bookmark.id}`}>
                    {bookmark.bookmark.name?.slice(0, 30)}
                  </Link>
                </h5>
                <div className="footer__bookmark">
                  <span className="span__type">{bookmark.bookmark.type}</span>
                  <button
                    type="button"
                    aria-label={`delete-${bookmark.bookmark.id}`}
                    onClick={() => dispatch(removeBookmark(index))}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};
export default Bookmarks;
