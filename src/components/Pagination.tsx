import React, { useState, useEffect } from 'react';

type Pages<T> = {
  itemsPerPage: T;
  totalItems: T;
  paginate: (pageNumber: number) => void;
  currentPage: T;
};

function Pagination<T extends number>({
  itemsPerPage,
  totalItems,
  paginate,
  currentPage,
}: Pages<T>) {
  const [pageNumbers, setPageNumbers] = useState<number[]>([]);

  useEffect(() => {
    const total: number = Math.ceil(+totalItems / +itemsPerPage);
    setPageNumbers(Array.from({ length: total }, (_, index) => index + 1));
  }, [totalItems, itemsPerPage]);

  return (
    <ul className="pagination">
      {pageNumbers.map(number => (
        <li key={number}>
          <button
            aria-label={`Page ${number}`}
            className={`${currentPage === number ? 'currentPage' : ''}`}
            onClick={() => paginate(number)}
            type="button"
          >
            {number}
          </button>
        </li>
      ))}
    </ul>
  );
}

export default Pagination;
