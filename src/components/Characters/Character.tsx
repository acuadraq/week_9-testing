/* eslint-disable operator-linebreak */
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { ICharacter, IResults } from '../../interfaces/characters';
import { AppState } from '../../redux/reducers';
import Pagination from '../Pagination';

type characterProps = {
  characterArray: ICharacter | undefined;
};

const Character = ({ characterArray }: characterProps) => {
  const characters: IResults[] | undefined = characterArray?.data.results;
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const charactersPerPage: number = 9;

  const paginate = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  const indexOfLastChar: number = currentPage * charactersPerPage;
  const indexOfFirstChar: number = indexOfLastChar - charactersPerPage;
  const currentCharacters: IResults[] | undefined = characters?.slice(
    indexOfFirstChar,
    indexOfLastChar,
  );

  useEffect(() => {
    setCurrentPage(1);
  }, [characters]);

  return (
    <div>
      <div className="items__container">
        {currentCharacters?.length === 0 && <p>No characters found</p>}
        {currentCharacters &&
          currentCharacters.map(character => (
            <div
              key={character.id}
              className={`item__div ${
                resourceHidden.includes(character.id) ? 'display__none' : ''
              }`}
            >
              <div className="image__container">
                <Link to={`${character.id}`}>
                  <img
                    src={`${character.thumbnail?.path}.${character.thumbnail?.extension}`}
                    alt=""
                  />
                </Link>
              </div>
              <div className="text__container">
                <h5>
                  <Link to={`${character.id}`}>{character.name}</Link>
                </h5>
              </div>
            </div>
          ))}
      </div>
      <Pagination
        itemsPerPage={charactersPerPage}
        totalItems={characters?.length || 0}
        paginate={paginate}
        currentPage={currentPage}
      />
    </div>
  );
};

export default Character;
