/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import { Link } from 'react-router-dom';
import spiderman from './spiderman.jpg';
import eros from './eros.png';
import carnage from './carnage.jpg';
import morbius from './morbius.jpg';
import venom from './venom.jpg';
import RoutesNames from '../../customroutes/routesNames';

const Home = () => {
  return (
    <>
      <div className="banner">
        <div className="banner__container">
          <div className="banner__content">
            <h1>Eternals</h1>
            <p>Now in theaters</p>
            <a
              href="https://www.youtube.com/watch?v=0WVDKZJkGlY&ab_channel=MarvelEntertainment"
              target="_blank"
            >
              Watch Trailer
            </a>
          </div>
        </div>
      </div>
      <section>
        <div className="container">
          <h3>Upcoming Film</h3>
          <hr />
          <div className="films__container">
            <div className="film">
              <div className="film__content">
                <h2>Spider-Man No Way Home</h2>
                <h3>Overview</h3>
                <hr />
                <p>
                  With Spider-Mans identity now revealed, our friendly neighborhood web-slinger is
                  unmasked and no longer able to separate his normal life as Peter Parker from the
                  high stakes of being a superhero. When Peter asks for help from Doctor Strange,
                  the stakes become even more dangerous, forcing him to discover what it truly means
                  to be Spider-Man.
                </p>
                <div className="film__li">
                  <div>
                    <h4>Director</h4>
                    <ul>
                      <li>Jon Watts</li>
                    </ul>
                  </div>
                  <div>
                    <h4>Cast</h4>
                    <ul>
                      <li>Tom Holland</li>
                      <li>Andrew Garfield</li>
                      <li>Tobey Maguire</li>
                    </ul>
                  </div>
                  <div>
                    <h4>Release Date</h4>
                    <ul>
                      <li>December 17, 2021</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="film__image-container">
                <img src={spiderman} alt="" />
              </div>
            </div>
          </div>
          <div className="flex__class">
            <h3>Featured Characters</h3>
            <Link to={RoutesNames.characters}>View All</Link>
          </div>
          <hr />
          <div className="characters__container">
            <div className="character__card">
              <div className="image__container">
                <img src={eros} alt="" />
              </div>
              <div className="character__text">
                <h4>Starfox (Eros)</h4>
              </div>
            </div>
            <div className="character__card">
              <div className="image__container">
                <img src={morbius} alt="" />
              </div>
              <div className="character__text">
                <h4>Morbius</h4>
              </div>
            </div>
            <div className="character__card">
              <div className="image__container">
                <img src={carnage} alt="" />
              </div>
              <div className="character__text">
                <h4>Carnage</h4>
              </div>
            </div>
            <div className="character__card">
              <div className="image__container">
                <img src={venom} alt="" />
              </div>
              <div className="character__text">
                <h4>Venom</h4>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default Home;
