import React, { useState, useEffect } from 'react';
import { getStories } from '../../adapters/Fetch';
import { IStories } from '../../interfaces/stories';
import Filter from './Filter';
import Story from './Story';

const Stories = () => {
  const [loading, setLoading] = useState(false);
  const [stories, setStories] = useState<IStories>();
  const [error, setError] = useState<string>('');

  const getStoriesFetch = (queries?: string) => {
    getStories(queries)
      .then(resp => {
        setStories(resp);
        setLoading(false);
      })
      .catch(err => {
        setError('There was an error, try again');
        setLoading(false);
      });
  };

  useEffect(() => {
    setLoading(true);
    getStoriesFetch();
  }, []);

  return (
    <section>
      <div className="container">
        <div className="grid__container">
          <Filter />
          {loading && <div className="animation__loading" aria-label="Loading" />}
          {error && <p>{error}</p>}
          <Story storyArray={stories} />
        </div>
      </div>
    </section>
  );
};
export default Stories;
