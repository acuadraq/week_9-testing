/* eslint-disable operator-linebreak */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Pagination from '../Pagination';
import { IStories, IStoriesResult } from '../../interfaces/stories';
import { AppState } from '../../redux/reducers';

type storyProps = {
  storyArray: IStories | undefined;
};

const Story = ({ storyArray }: storyProps) => {
  const stories: IStoriesResult[] | undefined = storyArray?.data.results;
  const [currentPage, setCurrentPage] = useState<number>(1);
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);
  const storiesPerPage: number = 9;
  const urlSearch = new URL(window.location.href);
  let storiesLength: number | undefined = stories?.length;

  const paginate = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  const getCurrentStories = (): IStoriesResult[] | undefined => {
    const param = urlSearch.searchParams.get('title');
    if (param) {
      const storiesFilter = stories?.filter(story => story.title.toLowerCase().includes(param));
      storiesLength = storiesFilter?.length;
      return storiesFilter?.slice(indexOfFirstStory, indexOfLastStory);
    }
    return stories?.slice(indexOfFirstStory, indexOfLastStory);
  };

  const indexOfLastStory: number = currentPage * storiesPerPage;
  const indexOfFirstStory: number = indexOfLastStory - storiesPerPage;
  const currentStories: IStoriesResult[] | undefined = getCurrentStories();

  return (
    <div>
      <div className="items__container">
        {currentStories?.length === 0 && <p>No Stories found</p>}
        {currentStories &&
          currentStories.map(story => (
            <div
              key={story.id}
              className={`item__div ${resourceHidden.includes(story.id) ? 'display__none' : ''}`}
            >
              <div className="image__container">
                <Link to={`${story.id}`}>
                  <img src="https://m.media-amazon.com/images/I/51B+S8JiAaL.jpg" alt="" />
                </Link>
              </div>
              <div className="text__container">
                <h5 aria-label={`${story.title.slice(0, 15)}`}>
                  <Link to={`${story.id}`}>
                    {story.title?.slice(0, 30)}
                    ...
                  </Link>
                </h5>
              </div>
            </div>
          ))}
      </div>
      <Pagination
        itemsPerPage={storiesPerPage}
        totalItems={storiesLength || 0}
        paginate={paginate}
        currentPage={currentPage}
      />
    </div>
  );
};

export default Story;
