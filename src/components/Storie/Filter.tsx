import React from 'react';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';

const Filter = () => {
  const urlSearch = new URL(window.location.href);
  const navigate = useNavigate();

  const setTitle = (value: string) => {
    urlSearch.searchParams.set('title', value);
    navigate(`${urlSearch.search}`);
  };

  return (
    <div className="filter__container">
      <p>Filter By:</p>
      <hr />
      <div>
        <label htmlFor="title">
          Title
          <input
            type="text"
            name="title"
            id="title"
            placeholder="Stories name"
            onChange={e => setTitle(e.target.value)}
          />
        </label>
      </div>
    </div>
  );
};

export default Filter;
