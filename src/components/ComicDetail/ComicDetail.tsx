/* eslint-disable react/jsx-no-target-blank */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { ICharacter } from '../../interfaces/characters';
import { IComic } from '../../interfaces/comics';
import { IStories } from '../../interfaces/stories';
import { removeBookmark, setBookmark, setHide } from '../../redux/actions/actions';
import NotFound from '../NotFound';
import bookmark from '../bookmark.png';
import { getComic, getComicCharacters, getComicStories } from '../../adapters/Fetch';
import ComicCharacters from './ComicCharacters';
import CharacterStories from '../CharacterDetail/CharacterStories';
import { AppState } from '../../redux/reducers';
import hide from '../hide.png';
import savedbookmark from '../saved-bookmark.png';
import useRefresh from '../../customhooks/useRefresh';
import filterId from '../../helpers/filterId';

type Bookmark = {
  bookmark: {
    id: number | undefined;
    name: string | undefined;
    thumbnail: string | undefined;
    type: string | undefined;
  };
};

const ComicDetail = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [comicInfo, setComicInfo] = useState<IComic>();
  const [characters, setCharacters] = useState<ICharacter>();
  const [stories, setStories] = useState<IStories>();
  const [error, setError] = useState<boolean>(false);
  const dispatch = useDispatch();
  const resourceHidden: number[] = useSelector((state: AppState) => state.hides);
  const bookmarks: Bookmark[] = useSelector((state: AppState) => state.bookmarks);
  const { id } = useParams<string>();
  const navigate = useNavigate();
  const refresh = useRefresh(navigate, `/comics/${id}`);

  const getStories = async () => {
    if (id) {
      const story: IStories = await getComicStories(parseInt(id, 10));
      await setStories(story);
      setLoading(false);
    }
  };

  const getCharacters = async () => {
    if (id) {
      const character: ICharacter = await getComicCharacters(parseInt(id, 10));
      await setCharacters(character);
      getStories();
    }
  };

  const getComicInfo = async () => {
    if (id) {
      if (resourceHidden.includes(parseInt(id, 10))) {
        setError(true);
        setLoading(false);
        return;
      }
      const comic: IComic = await getComic(parseInt(id, 10));
      if (comic.code === 404) {
        setError(true);
        setLoading(false);
        return;
      }
      await setComicInfo(comic);
      getCharacters();
    }
  };

  const saveBookmark = () => {
    const comic = {
      bookmark: {
        id: comicInfo?.data.results[0].id,
        name: comicInfo?.data.results[0].title,
        thumbnail: `${comicInfo?.data.results[0].thumbnail.path}.${comicInfo?.data.results[0].thumbnail.extension}`,
        type: 'comics',
      },
    };
    dispatch(setBookmark(comic));
  };

  const hideElement = (num: number) => {
    const isInBookmark = filterId(bookmarks, num);
    dispatch(setHide(num));
    if (isInBookmark !== -1) dispatch(removeBookmark(isInBookmark));
    refresh();
  };

  const bookmarkButton = (num: number) => {
    const isInBookmark = filterId(bookmarks, num);
    if (isInBookmark !== -1) return true;
    return false;
  };

  useEffect(() => {
    setLoading(true);
    getComicInfo();
  }, []);
  return (
    <section>
      <div className="container">
        {loading && <div className="animation__loading" aria-label="Loading" />}
        {!loading && (
          <>
            {error ? (
              <NotFound />
            ) : (
              comicInfo?.data.results.map(comic => (
                <div key={comic.id}>
                  <div className="comic__banner">
                    <div className="image__container">
                      <img
                        src={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
                        alt={comic.title}
                      />
                    </div>
                    <div className="comic__content">
                      <h1>{comic.title}</h1>
                      <div className="overview-button">
                        <span className="overview">Overview</span>
                        <button
                          type="button"
                          aria-label="hide-item"
                          onClick={() => hideElement(comic.id)}
                        >
                          <span>Hide</span>
                          <img src={hide} alt="Hide" width="20" height="20" />
                        </button>
                      </div>
                      <div className="points">
                        <div>
                          <h4>Price</h4>
                          <ul>
                            <li>{`$${comic.prices[0].price}`}</li>
                          </ul>
                        </div>
                        <div>
                          <h4>Format</h4>
                          <ul>
                            <li>{comic.format}</li>
                          </ul>
                        </div>
                      </div>
                      <p>
                        {comic.description
                          ? comic.description
                          : 'This comic doesnt have a description'}
                      </p>
                      <div className="buttons">
                        <a href={comic.urls[0].url} target="_blank">
                          Learn More
                        </a>
                        {bookmarkButton(comic.id) ? (
                          <button type="button" onClick={saveBookmark} aria-label="item-saved">
                            <span>Saved</span>
                            <img src={savedbookmark} alt="Bookmark" width="20" height="20" />
                          </button>
                        ) : (
                          <button type="button" onClick={saveBookmark} aria-label="save-item">
                            <span>Save</span>
                            <img src={bookmark} alt="Bookmark" width="20" height="20" />
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="items__additionals">
                    <h2>
                      Characters of
                      {` ${comic.title}`}
                    </h2>
                    <hr />
                    {characters?.data.results.length === 0 ? (
                      <p>This comic doesnt have characters</p>
                    ) : (
                      <ComicCharacters characters={characters} />
                    )}
                  </div>
                  <div className="items__additionals">
                    <h2>
                      Stories of
                      {` ${comic.title}`}
                    </h2>
                    <hr />
                    {stories?.data.results.length === 0 ? (
                      <p>This comic doesnt have stories</p>
                    ) : (
                      <CharacterStories stories={stories} />
                    )}
                  </div>
                </div>
              ))
            )}
          </>
        )}
      </div>
    </section>
  );
};

export default ComicDetail;
