/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import RoutesNames from '../customroutes/routesNames';
import { AppState } from '../redux/reducers';

const Navbar = () => {
  const [burguer, setBurguer] = useState<boolean>(false);
  const resourceHidden: number[] = useSelector((state: AppState) => state.bookmarks);

  return (
    <nav>
      <div className="logo">
        <Link to={RoutesNames.home}>
          <h4>Marvel</h4>
        </Link>
      </div>
      <ul className={`nav__links ${burguer ? 'nav-active' : ''}`}>
        <li>
          <Link to={RoutesNames.home}>Home</Link>
        </li>
        <li>
          <Link to={RoutesNames.characters}>Characters</Link>
        </li>
        <li>
          <Link to={RoutesNames.comics}>Comics</Link>
        </li>
        <li>
          <Link to={RoutesNames.stories}>Stories</Link>
        </li>
        <li className="last">
          <Link to={RoutesNames.bookmarks} className="bookmark_nav">
            Bookmarks
          </Link>
          <span>{resourceHidden.length}</span>
        </li>
      </ul>
      <div className="burguer" onClick={() => setBurguer(!burguer)}>
        <div className="line1" />
        <div className="line2" />
        <div className="line3" />
      </div>
    </nav>
  );
};

export default Navbar;
