import { RootStateOrAny } from 'react-redux';
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { bookmarkReducer, hideReducer } from './dataReducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['bookmarks', 'hides'],
};

export const rootReducer: RootStateOrAny = combineReducers({
  bookmarks: bookmarkReducer,
  hides: hideReducer,
});

const persistedReducer = persistReducer<RootStateOrAny>(persistConfig, rootReducer);

export type AppState = ReturnType<typeof rootReducer>;

export default persistedReducer;
