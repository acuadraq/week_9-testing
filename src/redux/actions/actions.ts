import ActionTypes from '../constants/action-types';

export type Bookmark = {
  bookmark: {
    id: number | undefined;
    name: string | undefined;
    thumbnail: string | undefined;
    type: string | undefined;
  };
};

export type SearchBookAction =
  | {
      type: ActionTypes.SET_BOOKMARK;
      payload: { bookmark: Bookmark };
    }
  | { type: ActionTypes.REMOVE_BOOKMARK; payload: { id: number } }
  | { type: ActionTypes.REMOVE_ALL_BOOKMARKS };

export const setBookmark = (bookmark: Bookmark) => {
  return {
    type: ActionTypes.SET_BOOKMARK,
    payload: { bookmark },
  };
};

export const removeBookmark = (id: number) => {
  return {
    type: ActionTypes.REMOVE_BOOKMARK,
    payload: { id },
  };
};

export const removeAllBookmarks = () => {
  return {
    type: ActionTypes.REMOVE_ALL_BOOKMARKS,
  };
};

export type HideAction = { type: ActionTypes.SET_HIDE; payload: { id: number } };

export const setHide = (id: number) => {
  return {
    type: ActionTypes.SET_HIDE,
    payload: { id },
  };
};
