import { compose, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import persistedReducer from './reducers/index';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__?: typeof compose;
  }
}

/* eslint-disable no-underscore-dangle */
export const store = createStore(
  persistedReducer,
  {},
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
/* eslint-enable */

export const persistor = persistStore(store);
