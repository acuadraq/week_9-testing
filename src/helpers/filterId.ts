import { Bookmark } from '../redux/actions/actions';

const filterId = (array: Bookmark[], id: number | undefined) => {
  return array.map(e => e.bookmark.id).indexOf(id);
};

export default filterId;
