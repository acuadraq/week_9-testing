import { useEffect } from 'react';
import RoutesNames from '../customroutes/routesNames';

export default function useRefresh(
  navigate: any,
  path: string,
  resetRoute: string = RoutesNames.home,
) {
  let handler: any;

  const refresh = () => {
    navigate(resetRoute);

    handler = setTimeout(() => navigate(path), 10);
  };

  useEffect(() => {
    return () => handler && clearTimeout(handler);
  }, [handler]);

  return refresh;
}
