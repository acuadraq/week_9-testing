/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/react-in-jsx-scope */
import { FunctionComponent, ReactElement, ReactNode } from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from '../redux/store';

type PropT = {
  children: ReactNode;
};

const LocationDisplay = () => {
  const location = useLocation();
  return <div data-testid="location-display">{location.pathname}</div>;
};

const Wrapper = ({ children }: PropT) => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <BrowserRouter>
          {children}
          <LocationDisplay />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
};

const renderWithRouter = (ui: ReactElement, route: string = '/') => {
  window.history.pushState({}, 'Test Page', route);
  return render(ui, { wrapper: Wrapper as FunctionComponent });
};

export default renderWithRouter;
