# Week_8 Marvel API

![](public/Pic.png)

## Description

This is the weekly assignment for the nine week. Its testing the page from the last week. This are the main points of the page:

- There are tests for every list (comics, characters, stories, bookmarks)
- Every api call is mocked
- The custom hook is tested
- The helper function is tested
- Every reducer is tested
- Every detail page is tested

If you want to tested in real time use this url: https://week-9-testing.vercel.app/
